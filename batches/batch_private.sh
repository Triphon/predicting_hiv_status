#!/bin/sh 
#SBATCH --partition=private-isg-cpu
#SBATCH --time=168:00:00
#SBATCH --job-name=predicting_hiv_status
#SBATCH --mail-type=ALL
#SBATCH --output=output_%j.txt
#SBATCH --error=log_%j.txt
#SBATCH --cpus-per-task=64
#SBATCH --mem=0

module load GCCcore/8.3.0 Python/3.7.4
. ~/virtualenvs/predicting_hiv_status/bin/activate

export XDG_RUNTIME_DIR=""

# specify here the directory containing your notebooks
JUPYTERLAB_DIR="~/predicting_hiv_status"

# launch Jupyter notebook
srun jupyter lab --no-browser --ip=$SLURMD_NODENAME --notebook-dir=$JUPYTERLAB_DIR
