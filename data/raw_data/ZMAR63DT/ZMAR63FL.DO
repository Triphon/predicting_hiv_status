infix using "C:\COUNTRIES\ZM63\ZMAR63FL.dct"

label variable hivclust "Cluster"
label variable hivnumb  "Household"
label variable hivline  "Line"
label variable hiv01    "Bar code"
label variable hiv02    "Lab DBS number"
label variable hiv03    "Blood test result"
label variable hiv05    "Sample weight"
label variable shiv50   "CD4 test result"
label variable shiv51   "Confirmed HIV status"

#delimit ;
label define HIV03   
     0 "HIV negative"
     1 "HIV  positive"
     2 "HIV2 positive"
     3 "HIV1 & HIV2 positive"
     4 "ERROR : V-, W+, M+"
     5 "ERROR : V-, W+, M-"
     6 "ERROR : V-, W-, M+"
     7 "Indeterminant"
;
label define SHIV51  
     0 "HIV negative"
     1 "HIV  positive"
     7 "Indeterminant"
     9 "Inconclusive"
;

#delimit cr
label values hiv03    HIV03   
label values shiv51   SHIV51  
