# import libraries
import os
import pandas as pd
import pathlib
import glob
import pickle
import shapefile

# set working directory and data folder
working_directory = '/home/oreler/predicting_hiv_status'
os.chdir(working_directory)
data_folder = '/home/oreler/predicting_hiv_status/data'

# set random seed
random_seed = 5
max_nan_percent = 0.3


# function for finding and removing element in a list
def find_and_remove_element(element, listname):
    s = set(listname)
    if element in s:
        listname.remove(element)
    else:
        print('element not in the list')


# importing HIV test results (AR data) for all countries from the data folder
AR_data_ = {}
for file in pathlib.Path(data_folder).glob('*AR*/*AR*.DTA'):
    print(file)
    data = os.fspath(file)
    name = file.name
    col_names = pd.read_stata(data, convert_categoricals=False).columns.tolist()
    error = True
    while error:
        try:
            pd.read_stata(data, columns=col_names)
        except ValueError as e:
            string = str(e)
            var = string.split()[4]
            find_and_remove_element(var, col_names)
        else:
            error = False
    AR_data_[name[0:2]] = pd.read_stata(data, columns=col_names)
    AR_data_[name[0:2]]['country'] = name[0:2]

# importing men (MR data) for all countries from the data folder
MR_data_ = {}
for file in pathlib.Path(data_folder).glob('*MR*/*MR*.DTA'):
    print(file)
    data = os.fspath(file)
    name = file.name
    col_names = pd.read_stata(data, convert_categoricals=False).columns.tolist()
    error = True
    while error:
        try:
            pd.read_stata(data, columns=col_names)
        except ValueError as e:
            string = str(e)
            var = string.split()[4]
            find_and_remove_element(var, col_names)
        else:
            error = False
    MR_data_[name[0:2]] = pd.read_stata(data, columns=col_names)
    MR_data_[name[0:2]]['country'] = name[0:2]
    
# importing women (IR data) for all countries from the data folder
IR_data_ = {}
for file in pathlib.Path(data_folder).glob('*IR*/*IR*.DTA'):
    print(file)
    data = os.fspath(file)
    name = file.name
    col_names = pd.read_stata(data, convert_categoricals=False).columns.tolist()
    error = True
    while error:
        try:
            pd.read_stata(data, columns=col_names)
        except ValueError as e:
            string = str(e)
            var = string.split()[4]
            find_and_remove_element(var, col_names)
        else:
            error = False
    IR_data_[name[0:2]] = pd.read_stata(data, columns=col_names)
    IR_data_[name[0:2]]['country'] = name[0:2]
    
# importing geolocalisation (GE data) for all countries from the data folder
GE_data_ = {}
for file in pathlib.Path(data_folder).glob('*GE*/*.shp'):
    print(file)
    sf = shapefile.Reader(str(file))
    fields = [x[0] for x in sf.fields][1:]
    records = sf.records()
    GE_data_[file.name[0:2]] = pd.DataFrame(columns=fields, data=records)

MRAR = {}
IRAR = {}

# merge individuals (men and women) per country with HIV status according to specific columns
for country in AR_data_.keys():
    MRAR[country] = MR_data_[country].merge(AR_data_[country].set_index(['hivclust', 'hivnumb', 'hivline', 'country']), right_index=True, left_on=['mv001', 'mv002', 'mv003', 'country'], how='inner')
    IRAR[country] = IR_data_[country].merge(AR_data_[country].set_index(['hivclust', 'hivnumb', 'hivline', 'country']), right_index=True, left_on=['v001', 'v002', 'v003', 'country'], how='inner')

MRGE = {}
IRGE = {}

# merge individuals (men and women) per country with geolocalisation according to specific columns
for country in AR_data_.keys():
    MRGE[country] = MRAR[country].merge(GE_data_[country].set_index(['DHSCLUST', 'DHSCC']), right_index=True, left_on=['mv001', 'country'], how='inner')
    IRGE[country] = IRAR[country].merge(GE_data_[country].set_index(['DHSCLUST', 'DHSCC']), right_index=True, left_on=['v001', 'country'], how='inner')

MRGE_resampled = {}
IRGE_resampled = {}

# sample individuals (men and women) per country according to hiv05 weights
for country in AR_data_.keys():
    MRGE_resampled[country] = MRGE[country].sample(frac=1, weights='hiv05', replace=True, random_state=random_seed)
    IRGE_resampled[country] = IRGE[country].sample(frac=1, weights='hiv05', replace=True, random_state=random_seed)

# concat countries datasets
MR_concat = pd.concat(MRGE_resampled.values(), join='inner', ignore_index=True)
IR_concat = pd.concat(IRGE_resampled.values(), join='inner', ignore_index=True)

# drop empty rows
MR_drop = MR_concat.dropna(axis='index', how='all')
IR_drop = IR_concat.dropna(axis='index', how='all')

# function for finding, counting and removing NaN based on a maximum 
def find_count_remove_missing(df, maximum=0.3):
    percent = (df.isnull().sum()/df.isnull().count()).sort_values(ascending=False)
    number = len(list(filter(lambda x: x>maximum, percent)))
    names = list(percent.keys()[:number])
    df = df.drop(names, 1)
    print(f'{number} columns excluded because haven\'t minimum data')
    return df

# remove columns with more than 30% NaN
MR_nan = find_count_remove_missing(MR_drop, max_nan_percent)
IR_nan = find_count_remove_missing(IR_drop, max_nan_percent)

# drop constant columns
MR_unique = MR_nan.loc[:, MR_drop.nunique()!=1]
IR_unique = IR_nan.loc[:, IR_drop.nunique()!=1]

# drop duplicate columns
MR_duplicate = MR_unique.T.drop_duplicates(keep='first').T
IR_duplicate = IR_unique.T.drop_duplicates(keep='first').T

# hiv03 # remove rows with 'indeterminate' and 'inconclusive' HIV tests # dummy
MR_inc = MR_duplicate[~(MR_duplicate['hiv03']=='inconclusive')].copy()
IR_inc = IR_duplicate[~(IR_duplicate['hiv03']=='inconclusive')].copy()

MR_ind = MR_inc[~(MR_inc['hiv03']=='indeterminate')].copy()
IR_ind = IR_inc[~(IR_inc['hiv03']=='indeterminate')].copy()
MR_ind = MR_ind[~(MR_ind['hiv03']=='indeterminant')].copy()
IR_ind = IR_ind[~(IR_ind['hiv03']=='indeterminant')].copy()

MR_null = MR_ind[~(MR_ind['hiv03'].isnull())].copy()
IR_null = IR_ind[~(IR_ind['hiv03'].isnull())].copy()

MR_null.loc[:, 'hiv03'] = MR_null['hiv03'].replace(('hiv negative', 'HIV negative'), 0)
MR_null.loc[:, 'hiv03'] = MR_null['hiv03'].replace(('hiv  positive', 'HIV  positive'), 1)
IR_null.loc[:, 'hiv03'] = IR_null['hiv03'].replace(('hiv negative', 'HIV negative'), 0)
IR_null.loc[:, 'hiv03'] = IR_null['hiv03'].replace(('hiv  positive', 'HIV  positive'), 1)

# v531 # remove 'not had sex' and replace 'inconsistent' and 'don\'t know' by NaN
MR_sex = MR_null[~(MR_null['mv531']=='not had sex')].copy()
IR_sex = IR_null[~(IR_null['v531']=='not had sex')].copy()

MR_sex.loc[:, 'mv531'] = MR_sex['mv531'].replace(('inconsistent', 'don\'t know'), float('NaN'))
IR_sex.loc[:, 'v531'] = IR_sex['v531'].replace(('inconsistent', 'don\'t know'), float('NaN'))

# pickling transformed data
f = open('data/processed/MR_processing.pkl', 'wb')
pickle.dump(MR_sex, f)
f = open('data/processed/IR_processing.pkl', 'wb')
pickle.dump(IR_sex, f)
f.close()