# import libraries
import os
import pandas as pd
import pathlib
import glob
import pickle
import re
import numpy as np

# set working directory
working_directory = '/home/oreler/predicting_hiv_status'
os.chdir(working_directory)

# load data
MR_sex = pickle.load(open('data/processed/MR_processing.pkl', 'rb'))

# mv012 # Current age
MR_sex.rename(columns={'mv012': 'Current age'}, inplace=True)

# mv025 # Type of place of residence # dummy # 0: rural - 1: urban
MR_sex['mv025'].replace('rural', 0, inplace=True)
MR_sex['mv025'].replace('urban', 1, inplace=True)
MR_sex.rename(columns={'mv025': 'Type of place of residence'}, inplace=True)

# mv035 # Number of wives/partners # male specific
MR_sex['mv035'].replace('no wives/partners', 0, inplace=True)
MR_sex.rename(columns={'mv035': 'Number of wives/partners'}, inplace=True)

# mv104 # Years lived in place of residence # 14443 NaNs
MR_sex['mv104'].replace('visitor', 0, inplace=True)
MR_sex.loc[MR_sex.loc[:, 'mv104']=='always', 'mv104'] = MR_sex.loc[:, 'Current age']
MR_sex.rename(columns={'mv104': 'Years lived in place of residence'}, inplace=True)

# mv106 # Highest educational level # ordinal # 0 to 3 # 10 NaNs
MR_sex['mv106'].replace('no education', 0, inplace=True)
MR_sex['mv106'].replace('primary', 1, inplace=True)
MR_sex['mv106'].replace('secondary', 2, inplace=True)
MR_sex['mv106'].replace('higher', 3, inplace=True)
MR_sex.rename(columns={'mv106': 'Highest educational level'}, inplace=True)

# mv130 # Religion # nominal # OneHot encoding # 67 NaNs
MR_sex['mv130'].replace('none', 'no religion', inplace=True)
MR_sex['mv130'].replace(('islamic', 'muslim', 'muslin', 'islam'), 'islam', inplace=True)
MR_sex['mv130'].replace(('elcin', 'protestant/anglican', 'ccap', 'anglican church', 'methodist',
                        'adventiste', 'seventh day adventist', 'seventh day adventist / baptist', 'seventh-day adventist', 'assembly of god', 'pentecostal',
                        'evangelical/pentecostal', 'lesotho evangelical church', 'seventh day adventist / baptist', 'protestant', 'anglican', 'adventist'), 'protestantism', inplace=True)
MR_sex['mv130'].replace(('roman catholic', 'roman catholic church', 'catholic'), 'catholicism', inplace=True)
MR_sex['mv130'].replace(('orthodox', 'jehovah witness', 'jehovah\'s witnesses'), 'other christian', inplace=True)
MR_sex['mv130'].replace(('universal', 'apostolic sect',  'animist', 'traditional', 'sect', 'zion'), 'other', inplace=True)
MR_sex = pd.get_dummies(MR_sex, columns=['mv130'], prefix=['Religion'], dummy_na=True)
for indice in list(MR_sex['Religion_nan'].loc[MR_sex['Religion_nan']==1].index):
    for colonne in list(MR_sex.columns):
        if re.match('Religion', colonne):
            MR_sex.loc[indice, colonne] = float('NaN')
MR_sex.drop('Religion_nan', axis='columns', inplace=True)

# mv133 # Total number of years of education # 18 NaNs
MR_sex.rename(columns={'mv133': 'Total number of years of education'}, inplace=True)

# mv135 # Usual resident or visitor # 8 NaNs
MR_sex['mv135'].replace('visitor', 0, inplace=True)
MR_sex['mv135'].replace('usual resident', 1, inplace=True)
MR_sex.rename(columns={'mv135': 'Usual resident or visitor'}, inplace=True)

# mv136 # Number of household members (total listed)
MR_sex.rename(columns={'mv136': 'Number of household members (total listed)'}, inplace=True)

# mv138 # Number of eligible men in household (de facto)
MR_sex.rename(columns={'mv138': 'Number of eligible men in household (de facto)'}, inplace=True)

# mv150 # Relationship to household head # OneHot Encoding # 3 NaNs
MR_sex['mv150'].replace(('son', 'son or daughter', 'son, daughter'), 'son/daughter', inplace=True)
MR_sex['mv150'].replace(('brother', 'brother or sister', 'brother, sister'), 'brother/sister', inplace=True)
MR_sex['mv150'].replace(('husband', 'wife or husband', 'husband or wife'), 'husband/wife', inplace=True)
MR_sex['mv150'].replace(('father-in-law', 'parent-in-law'), 'parent-in-law', inplace=True)
MR_sex['mv150'].replace(('son-in-law', 'son-in-law or daughter-in-law', 'son/daughter-in-law', 'son-in-law', 'daughter-in-law', 'son-in-law, daughter-in-law'), 'children-in-law', inplace=True)
MR_sex['mv150'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv150'].replace(('herdboy', 'domestic employee'), 'not related', inplace=True)
MR_sex['mv150'].replace(('adopted/foster child', 'grandchild', 'adopted/foster/step child', 'niece/nephew by blood', 'niece/nephew by marriage', 'grandson'), 'other relative', inplace=True)
MR_sex['mv150'].replace('father', 'parent', inplace=True)
MR_sex = pd.get_dummies(MR_sex, columns=['mv150'], prefix=['Relationship to household head'], dummy_na=True)
for indice in list(MR_sex['Relationship to household head_nan'].loc[MR_sex['Relationship to household head_nan']==1].index):
    for colonne in list(MR_sex.columns):
        if re.match('Relationship to household head', colonne):
            MR_sex.loc[indice, colonne] = float('NaN')
MR_sex.drop('Relationship to household head_nan', axis='columns', inplace=True)

# mv151 # Sex of household head
MR_sex['mv151'].replace('male', 0, inplace=True)
MR_sex['mv151'].replace('female', 1, inplace=True)
MR_sex.rename(columns={'mv151': 'Sex of household head'}, inplace=True)

# mv152 # Age of household head # numerical
MR_sex['mv152'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv152'].replace('95+', 100, inplace=True)
MR_sex.rename(columns={'mv152': 'Age of household head'}, inplace=True)

# mv155 # Literacy # ordinal # 0 to 2 # 4206 NaNs
MR_sex['mv155'].replace('no card with required language', float('NaN'), inplace=True)
MR_sex['mv155'].replace('able to read whole sentence', 2, inplace=True)
MR_sex['mv155'].replace(('blind/visually impaired', 'cannot read at all'), 0, inplace=True)
MR_sex['mv155'].replace(('able to read only parts of sentence'), 1, inplace=True)
MR_sex.rename(columns={'mv155': 'Literacy'}, inplace=True)

# mv157 # Frequency of reading newspaper or magazine # ordinal # 0 to 3 # 21 NaNs
MR_sex['mv157'].replace('not at all', 0, inplace=True)
MR_sex['mv157'].replace('less than once a week', 1, inplace=True)
MR_sex['mv157'].replace('at least once a week', 2, inplace=True)
MR_sex['mv157'].replace('almost every day', 3, inplace=True)
MR_sex.rename(columns={'mv157': 'Frequency of reading newspaper or magazine'}, inplace=True)

# mv158 # Frequency of listening to radio # ordinal # 0 to 3 # 30 NaNs
MR_sex['mv158'].replace('not at all', 0, inplace=True)
MR_sex['mv158'].replace('less than once a week', 1, inplace=True)
MR_sex['mv158'].replace('at least once a week', 2, inplace=True)
MR_sex['mv158'].replace('almost every day', 3, inplace=True)
MR_sex.rename(columns={'mv158': 'Frequency of listening to radio'}, inplace=True)

# mv159 # Frequency of watching television # ordinal # 0 to 3 # 25 NaNs
MR_sex['mv159'].replace('not at all', 0, inplace=True)
MR_sex['mv159'].replace('less than once a week', 1, inplace=True)
MR_sex['mv159'].replace('at least once a week', 2, inplace=True)
MR_sex['mv159'].replace('almost every day', 3, inplace=True)
MR_sex.rename(columns={'mv159': 'Frequency of watching television'}, inplace=True)

# mv167 # Times away from home in last 12 months # 94 NaNs
MR_sex['mv167'].replace(('90+', '95+'), 146, inplace=True)
MR_sex['mv167'].replace('none', 0, inplace=True)
MR_sex.rename(columns={'mv167': 'Times away from home in last 12 months'}, inplace=True)

# mv190 # Wealth index combined # ordinal # 0 to 4
MR_sex['mv190'].replace('poorest', 0, inplace=True)
MR_sex['mv190'].replace('poorer', 1, inplace=True)
MR_sex['mv190'].replace('middle', 2, inplace=True)
MR_sex['mv190'].replace('richer', 3, inplace=True)
MR_sex['mv190'].replace('richest', 4, inplace=True)
MR_sex.rename(columns={'mv190': 'Wealth index combined'}, inplace=True)

# mv191 # Wealth index factor score combined
MR_sex.rename(columns={'mv191': 'Wealth index factor score combined'}, inplace=True)

# mv202 # Sons at home # 405 NaNs
MR_sex.rename(columns={'mv202': 'Sons at home'}, inplace=True)

# mv203 # Daughters at home # 405 NaNs
MR_sex.rename(columns={'mv203': 'Daughters at home'}, inplace=True)

# mv204 # Sons elsewhere # 405 NaNs
MR_sex.rename(columns={'mv204': 'Sons elsewhere'}, inplace=True)

# mv205 # Daughters elsewhere # 405 NaNs
MR_sex.rename(columns={'mv205': 'Daughters elsewhere'}, inplace=True)

# mv206 # Sons who have died # 405 NaNs
MR_sex.rename(columns={'mv206': 'Sons who have died'}, inplace=True)

# mv207 # Daughters who have died # 405 NaNs
MR_sex.rename(columns={'mv207': 'Daughters who have died'}, inplace=True)

# mv217 # Knowledge of ovulatory cycle # 4482 NaNs
MR_sex = pd.get_dummies(MR_sex, columns=['mv217'], prefix=['Knowledge of ovulatory cycle'], dummy_na=True)
for indice in list(MR_sex['Knowledge of ovulatory cycle_nan'].loc[MR_sex['Knowledge of ovulatory cycle_nan']==1].index):
    for colonne in list(MR_sex.columns):
        if re.match('Knowledge of ovulatory cycle', colonne):
            MR_sex.loc[indice, colonne] = float('NaN')
MR_sex.drop('Knowledge of ovulatory cycle_nan', axis='columns', inplace=True)

# mv245 # Number of women fathered children with # 427 NaNs # 'more than one woman' replace by median above 1 i.e. 2
MR_sex['mv245'].replace('more than one woman, but unkknown number', 2, inplace=True)
MR_sex.rename(columns={'mv245': 'Number of women fathered children with'}, inplace=True)

# mv301 # Knowledge of any contraceptive method # ordinal # 0 to 3 # 4458 NaNs
MR_sex['mv301'].replace('knows no method', 0, inplace=True)
MR_sex['mv301'].replace('knows only folkloric method', 1, inplace=True)
MR_sex['mv301'].replace('knows only traditional method', 2, inplace=True)
MR_sex['mv301'].replace('knows modern method', 3, inplace=True)
MR_sex.rename(columns={'mv301': 'Knowledge of any contraceptive method'}, inplace=True)

# mv312 # Current contraceptive method # condom & male condom & female condom: 1 - other: 0 # dummy
MR_sex['mv312'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv312'].replace(float('NaN'), -1, inplace=True)
searchfor = ['condom', 'male condom', 'female condom', -1]
MR_sex['mv312'].replace(list(set(list(MR_sex['mv312'].unique()))-set(searchfor)), 0, inplace=True)
MR_sex['mv312'].replace(('condom', 'male condom', 'female condom'), 1, inplace=True)
MR_sex['mv312'].replace(-1, float('NaN'), inplace=True)
MR_sex.rename(columns={'mv312': 'Current contraceptive method'}, inplace=True)

# mv313 # Current contraceptive by method type # ordinal # 0 to 3
MR_sex['mv313'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv313'].replace('no method', 0, inplace=True)
MR_sex['mv313'].replace('folkloric method', 1, inplace=True)
MR_sex['mv313'].replace('traditional method', 2, inplace=True)
MR_sex['mv313'].replace('modern method', 3, inplace=True)
MR_sex.rename(columns={'mv313': 'Current contraceptive by method type'}, inplace=True)

# mv384a # Heard family planning on radio last few months # dummy # 4460 NaNs
MR_sex['mv384a'].replace('no', 0, inplace=True)
MR_sex['mv384a'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv384a': 'Heard family planning on radio last few months'}, inplace=True)

# mv384b # Heard family planning on TV last few months # dummy # 4469 NaNs
MR_sex['mv384b'].replace('no', 0, inplace=True)
MR_sex['mv384b'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv384b': 'Heard family planning on TV last few months'}, inplace=True)

# mv384c # Heard family planning in newspaper/magazine last few months # dummy # 4490 NaNs
MR_sex['mv384c'].replace('no', 0, inplace=True)
MR_sex['mv384c'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv384c': 'Heard family planning in newspaper/magazine last few months'}, inplace=True)

# mv395 # Discussed Family Planning with health worker in last few months # dummy # 4570 NaNs
MR_sex['mv395'].replace('no', 0, inplace=True)
MR_sex['mv395'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv395': 'Discussed Family Planning with health worker in last few months'}, inplace=True)

# mv3b25a # Contraception is woman's business, man should not worry # disagree: 0 - agree: 1 # dummy # 4467 NaNs
MR_sex['mv3b25a'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv3b25a'].replace('disagree', 0, inplace=True)
MR_sex['mv3b25a'].replace('agree', 1, inplace=True)
MR_sex.rename(columns={'mv3b25a': 'Contraception is woman\'s business, man should not worry'}, inplace=True)

# mv3b25b # Women who use contraception become promiscuous # disagree: 0 - agree: 1 # dummy # 4472 NaNs
MR_sex['mv3b25b'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv3b25b'].replace('disagree', 0, inplace=True)
MR_sex['mv3b25b'].replace('agree', 1, inplace=True)
MR_sex.rename(columns={'mv3b25b': 'Women who use contraception become promiscuous'}, inplace=True)

# mv477 # Number of injections in last 12 months # 5 NaNs
MR_sex['mv477'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv477'].replace('90+', 146, inplace=True)
MR_sex['mv477'].replace('none', 0, inplace=True)
MR_sex.rename(columns={'mv477': 'Number of injections in last 12 months'}, inplace=True)

# mv481 # Covered by health insurance # dummy # 4621 NaNs
MR_sex['mv481'].replace('no', 0, inplace=True)
MR_sex['mv481'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv481': 'Covered by health insurance'}, inplace=True)

# mv483 # Respondent circumcised # dummy # 6 NaNs
MR_sex['mv483'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv483'].replace('no', 0, inplace=True)
MR_sex['mv483'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv483': 'Respondent circumcised'}, inplace=True)

# mv502 # Currently/formerly/never in union
MR_sex['mv502'].replace('never in union', 0, inplace=True)
MR_sex['mv502'].replace('formerly in union/living with a woman', 1, inplace=True)
MR_sex['mv502'].replace('currently in union/living with a woman', 2, inplace=True)
MR_sex.rename(columns={'mv502': 'Currently/formerly/never in union'}, inplace=True)

# mv513 # Cohabitation duration (grouped)
MR_sex['mv513'].replace('never married', 0, inplace=True)
MR_sex['mv513'].replace('0-4', 1, inplace=True)
MR_sex['mv513'].replace('5-9', 2, inplace=True)
MR_sex['mv513'].replace('10-14', 3, inplace=True)
MR_sex['mv513'].replace('15-19', 4, inplace=True)
MR_sex['mv513'].replace('20-24', 5, inplace=True)
MR_sex['mv513'].replace('25-29', 6, inplace=True)
MR_sex['mv513'].replace('30+', 7, inplace=True)
MR_sex.rename(columns={'mv513': 'Cohabitation duration (grouped)'}, inplace=True)

# mv527 # Time since last sex (in days) # 40 NaNs
MR_sex['mv527'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv527'].replace('<1 day ago', 100, inplace=True)
MR_sex['mv527'].replace('days: 1', 101, inplace=True)
MR_sex['mv527'].replace('weeks: 1', 201, inplace=True)
MR_sex['mv527'].replace('months: 1', 301, inplace=True)
MR_sex['mv527'].replace('years: 1', 401, inplace=True)
MR_sex['mv527'].replace('years: number missing', np.mean(MR_sex.loc[MR_sex.mv527.astype('str').str.match('^4'), 'mv527']), inplace=True)
flag_1 = MR_sex.mv527.astype('str').str.match('^1')
flag_2 = MR_sex.mv527.astype('str').str.match('^2')
flag_3 = MR_sex.mv527.astype('str').str.match('^3')
flag_4 = MR_sex.mv527.astype('str').str.match('^4')
MR_sex.loc[flag_1, 'mv527'] = MR_sex.loc[flag_1, 'mv527'] - 100
MR_sex.loc[flag_2, 'mv527'] = (MR_sex.loc[flag_2, 'mv527'] - 200) * 7
MR_sex.loc[flag_3, 'mv527'] = (MR_sex.loc[flag_3, 'mv527'] - 300) * 30
MR_sex.loc[flag_4, 'mv527'] = round((MR_sex.loc[flag_4, 'mv527'] - 400) * 365)
MR_sex.rename(columns={'mv527': 'Time since last sex (in days)'}, inplace=True)

# mv531 # Age at first sex (imputed) # 958 NaNs
MR_sex.rename(columns={'mv531': 'Age at first sex (imputed)'}, inplace=True)

# mv536 # Recent sexual activity # dummy # 97 NaNs
MR_sex['mv536'].replace('not active in last 4 weeks', 0, inplace=True)
MR_sex['mv536'].replace('active in last 4 weeks', 1, inplace=True)
MR_sex.rename(columns={'mv536': 'Recent sexual activity'}, inplace=True)

# mv602 # Fertility preference # ordinal # 0 to 3 # 4516 NaNs
MR_sex['mv602'].replace(('declared infecund (respondent or partner(s))', 'sterilized (respondent or partner(s))'), 0, inplace=True)
MR_sex['mv602'].replace('no more', 1, inplace=True)
MR_sex['mv602'].replace(('undecided', 'man has no partner'), 2, inplace=True)
MR_sex['mv602'].replace('have another', 3, inplace=True)
MR_sex.rename(columns={'mv602': 'Fertility preference'}, inplace=True)

# mv613 # Ideal number of children # 4461 NaNs
MR_sex['mv613'].replace(('non-numeric response', 'god\'s will', 'don\'t know'), float('NaN'), inplace=True)
MR_sex.rename(columns={'mv613': 'Ideal number of children'}, inplace=True)

# mv627 # Ideal number of boys # 4461 NaNs
MR_sex['mv627'].replace(('other', 'god\'s will', 'don\'t know'), float('NaN'), inplace=True)
MR_sex.rename(columns={'mv627': 'Ideal number of boys'}, inplace=True)

# mv628 # Ideal number of girls # 4461 NaNs
MR_sex['mv628'].replace(('other', 'god\'s will', 'don\'t know'), float('NaN'), inplace=True)
MR_sex.rename(columns={'mv628': 'Ideal number of girls'}, inplace=True)

# mv629 # Ideal number of either sex # 4461 NaNs
MR_sex['mv629'].replace(('other', 'god\'s will', 'don\'t know'), float('NaN'), inplace=True)
MR_sex.rename(columns={'mv629': 'Ideal number of either sex'}, inplace=True)

# mv633b # Wife justified refusing sex: husband has other women # 24 NaNs
MR_sex['mv633b'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv633b'].replace('no', 0, inplace=True)
MR_sex['mv633b'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv633b': 'Wife justified refusing sex: husband has other women'}, inplace=True)

# mv714 # Currently working # dummy # 12 NaNs
MR_sex['mv714'].replace('no', 0, inplace=True)
MR_sex['mv714'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv714': 'Currently working'}, inplace=True)

# mv717 # Occupation (grouped) # onehot encoding # 767 NaNs
MR_sex['mv717'].replace(('others', 'armed forces'), 'other', inplace=True)
MR_sex['mv717'].replace(('agriculture - self employed', 'agriculture - employee'), 'agricultural', inplace=True)
MR_sex['mv717'].replace(('unskilled manual', 'skilled manual'), 'manual', inplace=True)
MR_sex['mv717'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex = pd.get_dummies(MR_sex, columns=['mv717'], prefix=['Occupation'], dummy_na=True)
for indice in list(MR_sex['Occupation_nan'].loc[MR_sex['Occupation_nan']==1].index):
    for colonne in list(MR_sex.columns):
        if re.match('Occupation', colonne):
            MR_sex.loc[indice, colonne] = float('NaN')
MR_sex.drop('Occupation_nan', axis='columns', inplace=True)

# mv731 # Respondent worked in last 7 days # ordinal # 2 NaN
MR_sex['mv731'].replace('no', 0, inplace=True)
MR_sex['mv731'].replace('in the past year', 1, inplace=True)
MR_sex['mv731'].replace('currently working', 2, inplace=True)
MR_sex.rename(columns={'mv731': 'Respondent worked in last 7 days'}, inplace=True)

# mv732 # Employment all year/seasonal # 6889 NaNs
MR_sex['mv732'].replace('temporary', 0, inplace=True)
MR_sex['mv732'].replace('occasional', 1, inplace=True)
MR_sex['mv732'].replace('seasonal', 2, inplace=True)
MR_sex['mv732'].replace('all year', 3, inplace=True)
MR_sex.rename(columns={'mv732': 'Employment all year/seasonal'}, inplace=True)

# mv741 # Type of earnings from respondent's work # ordinal # 0 to 3 # 6905 NaNs
MR_sex['mv741'].replace('not paid', 0, inplace=True)
MR_sex['mv741'].replace('in-kind only', 1, inplace=True)
MR_sex['mv741'].replace('cash and in-kind', 2, inplace=True)
MR_sex['mv741'].replace('cash only', 3, inplace=True)
MR_sex.rename(columns={'mv741': 'Type of earnings from respondent\'s work'}, inplace=True)

# mv744 # Beating justified # dummy (constructed) # 1 NaNs
MR_sex['mv744a'].replace('no', 0, inplace=True)
MR_sex['mv744b'].replace('no', 0, inplace=True)
MR_sex['mv744c'].replace('no', 0, inplace=True)
MR_sex['mv744d'].replace('no', 0, inplace=True)
MR_sex['mv744e'].replace('no', 0, inplace=True)
MR_sex['mv744a'].replace(('don\'t know', 'yes'), 1, inplace=True)
MR_sex['mv744b'].replace(('don\'t know', 'yes'), 1, inplace=True)
MR_sex['mv744c'].replace(('don\'t know', 'yes'), 1, inplace=True)
MR_sex['mv744d'].replace(('don\'t know', 'yes'), 1, inplace=True)
MR_sex['mv744e'].replace(('don\'t know', 'yes'), 1, inplace=True)
MR_sex['mv744a'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv744b'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv744c'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv744d'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv744e'].replace(float('NaN'), 0.1, inplace=True)
MR_sex.loc[:, 'mv744'] = round(MR_sex.loc[:, 'mv744a'] + MR_sex.loc[:, 'mv744b'] + MR_sex.loc[:, 'mv744c'] + MR_sex.loc[:, 'mv744d'] + MR_sex.loc[:, 'mv744e'], 1)
MR_sex.loc[:, 'mv744'] = np.where((MR_sex.loc[:, 'mv744']==0.3) , float('NaN'), MR_sex.loc[:, 'mv744'])
MR_sex.loc[:, 'mv744'] = np.where((MR_sex.loc[:, 'mv744']==0.4) , float('NaN'), MR_sex.loc[:, 'mv744'])
MR_sex.loc[:, 'mv744'] = np.where((MR_sex.loc[:, 'mv744']==0.5) , float('NaN'), MR_sex.loc[:, 'mv744'])
MR_sex.loc[:, 'mv744'] = np.where((MR_sex.loc[:, 'mv744']>=1), 1, MR_sex.loc[:, 'mv744'])
MR_sex.loc[:, 'mv744'] = np.where((MR_sex.loc[:, 'mv744']<=0.2), 0, MR_sex.loc[:, 'mv744'])
MR_sex.rename(columns={'mv744': 'Beating justified'}, inplace=True)
MR_sex.drop(['mv744a', 'mv744b', 'mv744c', 'mv744d', 'mv744e'], axis='columns', inplace=True)

# mv745a # Owns a house alone or jointly # ordinal # from 0 to 3 # 11 NaNs
MR_sex['mv745a'].replace('does not own', 0, inplace=True)
MR_sex['mv745a'].replace('jointly only', 1, inplace=True)
MR_sex['mv745a'].replace('alone only', 2, inplace=True)
MR_sex['mv745a'].replace('both alone and jointly', 3, inplace=True)
MR_sex.rename(columns={'mv745a': 'Owns a house alone or jointly'}, inplace=True)

# mv745b # Owns land alone or jointly # ordinal # from 0 to 3 # 36 NaNs
MR_sex['mv745b'].replace('does not own', 0, inplace=True)
MR_sex['mv745b'].replace('jointly only', 1, inplace=True)
MR_sex['mv745b'].replace('alone only', 2, inplace=True)
MR_sex['mv745b'].replace('both alone and jointly', 3, inplace=True)
MR_sex.rename(columns={'mv745b': 'Owns land alone or jointly'}, inplace=True)

# mv750 # Ever heard of a Sexually Transmitted Infection (STI) # dummy
MR_sex['mv750'].replace('no', 0, inplace=True)
MR_sex['mv750'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv750': 'Ever heard of a Sexually Transmitted Infection (STI)'}, inplace=True)

# mv751 # Ever heard of AIDS # dummy
MR_sex['mv751'].replace('no', 0, inplace=True)
MR_sex['mv751'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv751': 'Ever heard of AIDS'}, inplace=True)

# mv754 # Reduce risk of getting HIV # dummy (constructed) - from dropped mv754cp, mv754dp, mv756 (0 for 'no' & 'don't know' and 1 for 'yes') and mv754jp, mv754wp (0 for 'yes' & 'don't know' and 1 for 'no') # 634 NaNs
MR_sex['mv754cp'].replace(('don\'t know', 'no'), 0, inplace=True)
MR_sex['mv754cp'].replace('yes', 1, inplace=True)
MR_sex['mv754dp'].replace(('don\'t know', 'no'), 0, inplace=True)
MR_sex['mv754dp'].replace('yes', 1, inplace=True)
MR_sex['mv756'].replace(('don\'t know', 'no'), 0, inplace=True)
MR_sex['mv756'].replace('yes', 1, inplace=True)
MR_sex['mv823'].replace(('don\'t know', 'no'), 0, inplace=True)
MR_sex['mv823'].replace('yes', 1, inplace=True)
MR_sex['mv754jp'].replace(('don\'t know', 'yes'), 0, inplace=True)
MR_sex['mv754jp'].replace('no', 1, inplace=True)
MR_sex['mv754wp'].replace(('don\'t know', 'yes'), 0, inplace=True)
MR_sex['mv754wp'].replace('no', 1, inplace=True)
MR_sex['mv754cp'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv754dp'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv756'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv754jp'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv754wp'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv823'].replace(float('NaN'), 0.1, inplace=True)
MR_sex.loc[:, 'mv754'] = round(MR_sex.loc[:, 'mv754cp'] + MR_sex.loc[:, 'mv754dp'] + MR_sex.loc[:, 'mv754jp'] + MR_sex.loc[:, 'mv754wp'] + MR_sex.loc[:, 'mv756'] + MR_sex.loc[:, 'mv823'], 1)
MR_sex.loc[:, 'mv754'] = np.where((MR_sex.loc[:, 'mv754']==0.3) , float('NaN'), MR_sex.loc[:, 'mv754'])
MR_sex.loc[:, 'mv754'] = np.where((MR_sex.loc[:, 'mv754']==0.4) , float('NaN'), MR_sex.loc[:, 'mv754'])
MR_sex.loc[:, 'mv754'] = np.where((MR_sex.loc[:, 'mv754']==0.5) , float('NaN'), MR_sex.loc[:, 'mv754'])
MR_sex.loc[:, 'mv754'] = np.where((MR_sex.loc[:, 'mv754']==0.6) , float('NaN'), MR_sex.loc[:, 'mv754'])
MR_sex.loc[:, 'mv754'] = np.where((MR_sex.loc[:, 'mv754']>=1), 1, MR_sex.loc[:, 'mv754'])
MR_sex.loc[:, 'mv754'] = np.where((MR_sex.loc[:, 'mv754']<=0.2), 0, MR_sex.loc[:, 'mv754'])
MR_sex.rename(columns={'mv754': 'Reduce risk of getting HIV'}, inplace=True)
MR_sex.drop(['mv754cp', 'mv754dp', 'mv754jp', 'mv754wp', 'mv756', 'mv823'], axis='columns', inplace=True)

# mv761 # Condom used during last sex with most recent partner # dummy # 6015 NaNs
MR_sex['mv761'].replace('no', 0, inplace=True)
MR_sex['mv761'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv761': 'Condom used during last sex with most recent partner'}, inplace=True)

# mv763a # Had any STI in last 12 months # dummy # 87 NaNs
MR_sex['mv763a'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv763a'].replace('no', 0, inplace=True)
MR_sex['mv763a'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv763a': 'Had any STI in last 12 months'}, inplace=True)

# mv763b # Had genital sore/ulcer in last 12 months # dummy # 55 NaNs
MR_sex['mv763b'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv763b'].replace('no', 0, inplace=True)
MR_sex['mv763b'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv763b': 'Had genital sore/ulcer in last 12 months'}, inplace=True)

# mv763c # Had genital discharge in last 12 months # dummy # 51 NaNs
MR_sex['mv763c'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv763c'].replace('no', 0, inplace=True)
MR_sex['mv763c'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv763c': 'Had genital discharge in last 12 months'}, inplace=True)

# mv766b # Number of sex partners, including spouse, in last 12 months # 87 NaNs
MR_sex['mv766b'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex.rename(columns={'mv766b': 'Number of sex partners, including spouse, in last 12 months'}, inplace=True)

# mv767a # Relationship with most recent sex partner ordinal # from 0 to 5 # 6015 NaNs
MR_sex['mv767a'].replace('spouse', 0, inplace=True)
MR_sex['mv767a'].replace('live-in partner', 1, inplace=True)
MR_sex['mv767a'].replace(('girlfriend/fiance', 'girlfriend/fiancee'), 2, inplace=True)
MR_sex['mv767a'].replace('casual acquaintance', 3, inplace=True)
MR_sex['mv767a'].replace('other', 4, inplace=True)
MR_sex['mv767a'].replace('commercial sex worker', 5, inplace=True)
MR_sex.rename(columns={'mv767a': 'Relationship with most recent sex partner'}, inplace=True)

# mv774 # Ways of transmission from mother to child # dummy # 636 NaNs
MR_sex['mv774a'].replace(('don\'t know', 'no'), 1, inplace=True)
MR_sex['mv774a'].replace('yes', 0, inplace=True)
MR_sex['mv774b'].replace(('don\'t know', 'no'), 1, inplace=True)
MR_sex['mv774b'].replace('yes', 0, inplace=True)
MR_sex['mv774c'].replace(('don\'t know', 'no'), 1, inplace=True)
MR_sex['mv774c'].replace('yes', 0, inplace=True)
MR_sex['mv774a'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv774b'].replace(float('NaN'), 0.1, inplace=True)
MR_sex['mv774c'].replace(float('NaN'), 0.1, inplace=True)
MR_sex.loc[:, 'mv774'] = round(MR_sex.loc[:, 'mv774a'] + MR_sex.loc[:, 'mv774b'] + MR_sex.loc[:, 'mv774c'], 1)
MR_sex.loc[:, 'mv774'] = np.where((MR_sex.loc[:, 'mv774']==0.2) , float('NaN'), MR_sex.loc[:, 'mv774'])
MR_sex.loc[:, 'mv774'] = np.where((MR_sex.loc[:, 'mv774']==0.3) , float('NaN'), MR_sex.loc[:, 'mv774'])
MR_sex.loc[:, 'mv774'] = np.where((MR_sex.loc[:, 'mv774']>=1), 1, MR_sex.loc[:, 'mv774'])
MR_sex.loc[:, 'mv774'] = np.where((MR_sex.loc[:, 'mv774']<=0.1), 0, MR_sex.loc[:, 'mv774'])
MR_sex.rename(columns={'mv774': 'Ways of transmission from mother to child'}, inplace=True)
MR_sex.drop(['mv774a', 'mv774b', 'mv774c'], axis='columns', inplace=True)
                       
# mv781 # Ever been tested for HIV # dummy # 1 NaNs
MR_sex['mv781'].replace('no', 0, inplace=True)
MR_sex['mv781'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv781': 'Ever been tested for HIV'}, inplace=True)

# mv783 # Know a place to get HIV test # 3675 NaNs
MR_sex['mv783'].replace('no', 0, inplace=True)
MR_sex['mv783'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv783': 'Know a place to get HIV test'}, inplace=True)

# mv785 # Heard about other STIs # 58 NaNs
MR_sex['mv785'].replace('no', 0, inplace=True)
MR_sex['mv785'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv785': 'Heard about other STIs'}, inplace=True)

# mv791 # Have ever paid anyone in exchange for sex # 860 NaNs
MR_sex['mv791'].replace('no', 0, inplace=True)
MR_sex['mv791'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv791': 'Have ever paid anyone in exchange for sex'}, inplace=True)

# mv793 # Paid for sex in last 12 months # 857 NaNs
MR_sex['mv793'].replace('no', 0, inplace=True)
MR_sex['mv793'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv793': 'Paid for sex in last 12 months'}, inplace=True)

# mv822 # Wife justified asking husband to use condom if he has STI # 3 NaNs
MR_sex['mv822'].replace(('don\'t know', 'no'), 0, inplace=True)
MR_sex['mv822'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv822': 'Wife justified asking husband to use condom if he has STI'}, inplace=True)

# mv824 # Drugs to avoid HIV transmission to baby during pregnancy # 5002 NaNs
MR_sex['mv824'].replace(('don\'t know', 'no'), 0, inplace=True)
MR_sex['mv824'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv824': 'Drugs to avoid HIV transmission to baby during pregnancy'}, inplace=True)

# mv825 # Would buy vegetables from vendor with HIV # 636 NaNs
MR_sex['mv825'].replace(('don\'t know', 'no'), 0, inplace=True)
MR_sex['mv825'].replace('yes', 1, inplace=True)
MR_sex.rename(columns={'mv825': 'Would buy vegetables from vendor with HIV'}, inplace=True)

# mv834a # Age of most recent partner # 6093 NaNs
MR_sex['mv834a'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv834a'].replace('95+', 100, inplace=True)
MR_sex.rename(columns={'mv834a': 'Age of most recent partner'}, inplace=True)

# mv836 # Total lifetime number of sex partners # 24 NaNs
MR_sex['mv836'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv836'].replace('95+', 146, inplace=True)
MR_sex.rename(columns={'mv836': 'Total lifetime number of sex partners'}, inplace=True)

# mv853a # Times in last 12 months had sex with most recent partner # 6109 NaNs
MR_sex['mv853a'].replace('don\'t know', float('NaN'), inplace=True)
MR_sex['mv853a'].replace('95+', 146, inplace=True)
MR_sex.rename(columns={'mv853a': 'Times in last 12 months had sex with most recent partner'}, inplace=True)

# ALT_DEM # Cluster altitude in meters # 179 NaNs
MR_sex['ALT_DEM'].replace(9999, float('NaN'), inplace=True)
MR_sex.rename(columns={'ALT_DEM': 'Cluster altitude in meters'}, inplace=True)

# LATNUM # 
MR_sex.rename(columns={'LATNUM': 'Cluster\'s latitude coordinate'}, inplace=True)

# LONGNUM # Current age
MR_sex.rename(columns={'LONGNUM': 'Cluster\'s longitude coordinate'}, inplace=True)

MR_drop_list = ['mcaseid', 'mv000', 'mv001', 'mv002', 'mv003', 'mv005', 'mv006', 'mv007', 'mv008', 'mv009', 'mv010', 'mv011',
                'mv013', 'mv014', 'mv016', 'mv022', 'mv023', 'mv024', 'mv027', 'mv028', 'mv030', 'mv107', 'mv149', 'mv201',
                'mv218', 'm304a_18', 'mv304_01', 'mv304_02', 'mv304_03', 'mv304_04', 'mv304_05', 'mv304_06', 'mv304_07', 'mv304_08',
                'mv304_09', 'mv304_10', 'mv304_11', 'mv304_13', 'mv304_14', 'mv304_16', 'mv304_17', 'mv304_18', 'mv307_01', 'mv307_03',
                'mv307_05', 'mv307_14', 'mv307_15', 'mv481b', 'mv481d', 'mv481x', 'mv501', 'mv525', 'mv528', 'mv529', 'mv532', 'mv614',
                'mv766a', 'mv801', 'mv802', 'mv803', 'hiv01', 'hiv02', 'hiv05', 'DHSID', 'DHSYEAR', 'CCFIPS', 'ADM1FIPS', 'ADM1FIPSNA',
                'ADM1SALBNA', 'ADM1SALBCO', 'ADM1DHS', 'ADM1NAME', 'DHSREGCO', 'DHSREGNA', 'SOURCE', 'URBAN_RURA', 'ALT_GPS']
MR_sex = MR_sex.drop(MR_drop_list, axis='columns')

# pickling transformed data
f = open('data/processed/MR_features.pkl', 'wb')
pickle.dump(MR_sex, f)
f.close()