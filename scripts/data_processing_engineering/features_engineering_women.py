# import libraries
import os
import pandas as pd
import pathlib
import glob
import pickle
import re
import numpy as np

# set working directory
working_directory = '/home/oreler/predicting_hiv_status'
os.chdir(working_directory)

# load data
IR_sex = pickle.load(open('data/processed/IR_processing.pkl', 'rb'))

# v012 # Current age
IR_sex.rename(columns={'v012': 'Current age'}, inplace=True)

# v025 # Type of place of residence # dummy # 0: rural - 1: urban
IR_sex['v025'].replace('rural', 0, inplace=True)
IR_sex['v025'].replace('urban', 1, inplace=True)
IR_sex.rename(columns={'v025': 'Type of place of residence'}, inplace=True)

# v040 # Cluster altitude in meters
IR_sex.rename(columns={'v040': 'Cluster altitude in meters'}, inplace=True)

# v104 # Years lived in place of residence # 18789 NaN
IR_sex['v104'].replace('visitor', 0, inplace=True)
IR_sex.loc[IR_sex.loc[:, 'v104']=='always', 'v104'] = IR_sex.loc[:, 'Current age']
IR_sex.rename(columns={'v104': 'Years lived in place of residence'}, inplace=True)

# v106 # Highest educational level # 19 NaN
IR_sex['v106'].replace('no education', 0, inplace=True)
IR_sex['v106'].replace('primary', 1, inplace=True)
IR_sex['v106'].replace('secondary', 2, inplace=True)
IR_sex['v106'].replace('higher', 3, inplace=True)
IR_sex.rename(columns={'v106': 'Highest educational level'}, inplace=True)

# v115 # Time to get to water source # 37 NaN # female specific
IR_sex['v115'].replace('on premises', 0, inplace=True)
IR_sex['v115'].replace(('don\'t know', 'not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v115': 'Time to get to water source'}, inplace=True)

# v119 # Household has: electricity # 27 NaN
IR_sex['v119'].replace('no', 0, inplace=True)
IR_sex['v119'].replace('yes', 1, inplace=True)
IR_sex['v119'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v119': 'Household has: electricity'}, inplace=True)

# v120 # Household has: radio # 7 NaN
IR_sex['v120'].replace('no', 0, inplace=True)
IR_sex['v120'].replace('yes', 1, inplace=True)
IR_sex['v120'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v120': 'Household has: radio'}, inplace=True)

# v121 # Household has: television # 13 NaN
IR_sex['v121'].replace('no', 0, inplace=True)
IR_sex['v121'].replace('yes', 1, inplace=True)
IR_sex['v121'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v121': 'Household has: television'}, inplace=True)

# v122 # Household has: refrigerator" # 22 NaN
IR_sex['v122'].replace('no', 0, inplace=True)
IR_sex['v122'].replace('yes', 1, inplace=True)
IR_sex['v122'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v122': 'Household has: refrigerator'}, inplace=True)

# v123 # Household has: bicycle" # 13 NaN
IR_sex['v123'].replace('no', 0, inplace=True)
IR_sex['v123'].replace('yes', 1, inplace=True)
IR_sex['v123'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v123': 'Household has: bicycle'}, inplace=True)

# v124 # Household has: motorcycle/scooter # 15 NaN
IR_sex['v124'].replace('no', 0, inplace=True)
IR_sex['v124'].replace('yes', 1, inplace=True)
IR_sex['v124'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v124': 'Household has: motorcycle/scooter'}, inplace=True)

# v125 # Household has: car/truck # 16 NaN
IR_sex['v125'].replace('no', 0, inplace=True)
IR_sex['v125'].replace('yes', 1, inplace=True)
IR_sex['v125'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v125': 'Household has: car/truck'}, inplace=True)

# v130 # Religion # nominal # OneHot encoding # 48 NaN
IR_sex['v130'].replace('none', 'no religion', inplace=True)
IR_sex['v130'].replace(('islamic', 'muslim', 'muslin'), 'islam', inplace=True)
IR_sex['v130'].replace(('elcin', 'protestant/anglican', 'ccap', 'anglican church', 'methodist',
                        'adventiste', 'seventh day adventist', 'seventh day adventist / baptist', 'seventh-day adventist', 'assembly of god', 'pentecostal',
                        'evangelical/pentecostal', 'lesotho evangelical church', 'seventh day adventist / baptist', 'protestant', 'anglican', 'adventist'), 'protestantism', inplace=True)
IR_sex['v130'].replace(('roman catholic', 'roman catholic church', 'catholic'), 'catholicism', inplace=True)
IR_sex['v130'].replace(('orthodox', 'jehovah witness', 'jehovah\'s witnesses'), 'other christian', inplace=True)
IR_sex['v130'].replace(('universal', 'apostolic sect', 'animist', 'traditional', 'sect', 'zion'), 'other', inplace=True)
IR_sex = pd.get_dummies(IR_sex, columns=['v130'], prefix='Religion', dummy_na=True)
for indice in list(IR_sex['Religion_nan'].loc[IR_sex['Religion_nan']==1].index):
    for colonne in list(IR_sex.columns):
        if re.match('Religion', colonne):
            IR_sex.loc[indice, colonne] = float('NaN')
IR_sex.drop('Religion_nan', axis='columns', inplace=True)

# v133 # Total number of years of education #  # 27 NaN
IR_sex.rename(columns={'v133': 'Total number of years of education'}, inplace=True)

# v135 # Usual resident or visitor # 5 NaN
IR_sex['v135'].replace('visitor', 0, inplace=True)
IR_sex['v135'].replace('usual resident', 1, inplace=True)
IR_sex.rename(columns={'v135': 'Usual resident or visitor'}, inplace=True)

# v136 # Number of household members (total listed)
IR_sex.rename(columns={'v136': 'Number of household members (total listed)'}, inplace=True)

# v137 # Number of children 5 and under in household (de jure)
IR_sex.rename(columns={'v137': 'Number of children 5 and under in household (de jure)'}, inplace=True)

# v138 # Number of eligible women in household (de facto)
IR_sex.rename(columns={'v138': 'Number of eligible women in household (de facto)'}, inplace=True)

# v150 # Relationship to household head # OneHot Encoding # 4 NaN
IR_sex['v150'].replace('daughter', 'son/daughter', inplace=True)
IR_sex['v150'].replace('sister', 'brother/sister', inplace=True)
IR_sex['v150'].replace(('wife', 'wife or husband', 'co-spouse'), 'husband/wife', inplace=True)
IR_sex['v150'].replace('mother-in-law', 'parent-in-law', inplace=True)
IR_sex['v150'].replace(('son/daughter-in-law', 'daughter-in-law'), 'children-in-law', inplace=True)
IR_sex['v150'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v150'].replace('domestic employee', 'not related', inplace=True)
IR_sex['v150'].replace(('adopted/foster child', 'grandchild', 'granddaughter', 'niece/nephew by blood', 'niece/nephew by marriage'), 'other relative', inplace=True)
IR_sex['v150'].replace('mother', 'parent', inplace=True)
IR_sex = pd.get_dummies(IR_sex, columns=['v150'], prefix='Relationship to household head', dummy_na=True)
for indice in list(IR_sex['Relationship to household head_nan'].loc[IR_sex['Relationship to household head_nan']==1].index):
    for colonne in list(IR_sex.columns):
        if re.match('Relationship to household head', colonne):
            IR_sex.loc[indice, colonne] = float('NaN')
IR_sex.drop('Relationship to household head_nan', axis='columns', inplace=True)

# v151 # Sex of household head # dummy # 0: male - 1: female
IR_sex['v151'].replace('male', 0, inplace=True)
IR_sex['v151'].replace('female', 1, inplace=True)
IR_sex.rename(columns={'v151': 'Sex of household head'}, inplace=True)

# v152 # Age of household head # 1 NaN 
IR_sex['v152'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v152'].replace('95+', 100, inplace=True)
IR_sex.rename(columns={'v152': 'Age of household head'}, inplace=True)

# v153 # Household has: telephone (land-line) # 132 NaN
IR_sex['v153'].replace('no', 0, inplace=True)
IR_sex['v153'].replace('yes', 1, inplace=True)
IR_sex['v153'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v153': 'Household has: telephone (land-line)'}, inplace=True)

# v155 # Literacy # ordinale # 0 to 2 # 6553 NaN
IR_sex['v155'].replace('no card with required language', float('NaN'), inplace=True)
IR_sex['v155'].replace('able to read whole sentence', 2, inplace=True)
IR_sex['v155'].replace(('blind/visually impaired', 'cannot read at all'), 0, inplace=True)
IR_sex['v155'].replace(('able to read only parts of sentence'), 1, inplace=True)
IR_sex.rename(columns={'v155': 'Literacy'}, inplace=True)

# v157 # Frequency of reading newspaper or magazine # ordinale # 0 to 3 # 61 NaN
IR_sex['v157'].replace('not at all', 0, inplace=True)
IR_sex['v157'].replace('less than once a week', 1, inplace=True)
IR_sex['v157'].replace('at least once a week', 2, inplace=True)
IR_sex['v157'].replace('almost every day', 3, inplace=True)
IR_sex.rename(columns={'v157': 'Frequency of reading newspaper or magazine'}, inplace=True)

# v158 # Frequency of listening to radio # ordinale # 0 to 3 # 36 NaN
IR_sex['v158'].replace('not at all', 0, inplace=True)
IR_sex['v158'].replace('less than once a week', 1, inplace=True)
IR_sex['v158'].replace('at least once a week', 2, inplace=True)
IR_sex['v158'].replace('almost every day', 3, inplace=True)
IR_sex.rename(columns={'v158': 'Frequency of listening to radio'}, inplace=True)

# v159 # Frequency of watching television # ordinale # 0 to 3 # 32 NaN
IR_sex['v159'].replace('not at all', 0, inplace=True)
IR_sex['v159'].replace('less than once a week', 1, inplace=True)
IR_sex['v159'].replace('at least once a week', 2, inplace=True)
IR_sex['v159'].replace('almost every day', 3, inplace=True)
IR_sex.rename(columns={'v159': 'Frequency of watching television'}, inplace=True)

# v160 # Toilet facilities shared with other households # 13752 NaN
IR_sex['v160'].replace('no', 0, inplace=True)
IR_sex['v160'].replace('yes', 1, inplace=True)
IR_sex['v160'].replace(('not a dejure resident', 'not a de jure resident'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v160': 'Toilet facilities shared with other households'}, inplace=True)

# v167 # Times away from home in last 12 months #  # 11194 NaN
IR_sex['v167'].replace('90+', 146, inplace=True)
IR_sex['v167'].replace('none', 0, inplace=True)
IR_sex.rename(columns={'v167': 'Times away from home in last 12 months'}, inplace=True)

# v190 # Wealth index combined # ordinal # 0 to 4 # int64
IR_sex['v190'].replace('poorest', 0, inplace=True)
IR_sex['v190'].replace('poorer', 1, inplace=True)
IR_sex['v190'].replace('middle', 2, inplace=True)
IR_sex['v190'].replace('richer', 3, inplace=True)
IR_sex['v190'].replace('richest', 4, inplace=True)
IR_sex.rename(columns={'v190': 'Wealth index combined'}, inplace=True)

# v191 # Wealth index factor score combined (5 decimals)
IR_sex.rename(columns={'v191': 'Wealth index factor score combined'}, inplace=True)

# ml101 # Type of mosquito bed net(s) slept under last night # ordinal # 14079 NaN
IR_sex['ml101'].replace('no net', 0, inplace=True)
IR_sex['ml101'].replace('only untreated nets', 1, inplace=True)
IR_sex['ml101'].replace('both treated and untreated nets', 2, inplace=True)
IR_sex['ml101'].replace('only treated nets', 3, inplace=True)
IR_sex.rename(columns={'ml101': 'Type of mosquito bed net(s) slept under last night'}, inplace=True)

# v202 # Sons at home # 1508 NaN
IR_sex.rename(columns={'v202': 'Sons at home'}, inplace=True)

# v203 # Daughters at home # 1508 NaN
IR_sex.rename(columns={'v203': 'Daughters at home'}, inplace=True)

# v204 # Sons elsewhere # 1508 NaN
IR_sex.rename(columns={'v204': 'Sons elsewhere'}, inplace=True)

# v205 # Daughters elsewhere # 1508 NaN
IR_sex.rename(columns={'v205': 'Daughters elsewhere'}, inplace=True)

# v206 # Sons who have died # 1508 NaN
IR_sex.rename(columns={'v206': 'Sons who have died'}, inplace=True)

# v207 # Daughters who have died # 1508 NaN
IR_sex.rename(columns={'v207': 'Daughters who have died'}, inplace=True)

# v208 # Births in last five years # 1508 NaN
IR_sex['v208'].replace('no births', 0, inplace=True)
IR_sex.rename(columns={'v208': 'Births in last five years'}, inplace=True)

# v209 # Births in past year # 1508 NaN
IR_sex['v209'].replace('no births', 0, inplace=True)
IR_sex.rename(columns={'v209': 'Births in past year'}, inplace=True)

# v210 # Births in month of interview # 1508 NaN
IR_sex.rename(columns={'v210': 'Births in month of interview'}, inplace=True)

# v213 # Currently pregnant # 1508 NaN
IR_sex['v213'].replace('no or unsure', 0, inplace=True)
IR_sex['v213'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v213': 'Currently pregnant'}, inplace=True)

# v216 # Menstruated in last six weeks # 1508 NaN
IR_sex['v216'].replace('no', 0, inplace=True)
IR_sex['v216'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v216': 'Menstruated in last six weeks'}, inplace=True)

# v217 # Knowledge of ovulatory cycle # 7260 NaN
IR_sex = pd.get_dummies(IR_sex, columns=['v217'], prefix='Knowledge of ovulatory cycle', dummy_na=True)
for indice in list(IR_sex['Knowledge of ovulatory cycle_nan'].loc[IR_sex['Knowledge of ovulatory cycle_nan']==1].index):
    for colonne in list(IR_sex.columns):
        if re.match('Knowledge of ovulatory cycle', colonne):
            IR_sex.loc[indice, colonne] = float('NaN')
IR_sex.drop('Knowledge of ovulatory cycle_nan', axis='columns', inplace=True)

# v224 # Entries in birth history # 1508 NaN
IR_sex.rename(columns={'v224': 'Entries in birth history'}, inplace=True)

# v228 # Ever had a terminated pregnancy # 7208 NaN
IR_sex['v228'].replace('no', 0, inplace=True)
IR_sex['v228'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v228': 'Ever had a terminated pregnancy'}, inplace=True)

# v235 # Index last child prior to maternity-health (calendar) # 7203 NaN
IR_sex['v235'].replace('no prior child', 0, inplace=True)
IR_sex.rename(columns={'v235': 'Index last child prior to maternity-health (calendar)'}, inplace=True)

# v238 # Births in last three years # 1508 NaN
IR_sex.rename(columns={'v238': 'Births in last three years'}, inplace=True)

# v301 # Knowledge of any contraceptive method # 7203 NaN
IR_sex['v301'].replace('knows no method', 0, inplace=True)
IR_sex['v301'].replace('knows only folkloric method', 1, inplace=True)
IR_sex['v301'].replace('knows only traditional method', 2, inplace=True)
IR_sex['v301'].replace('knows modern method', 3, inplace=True)
IR_sex.rename(columns={'v301': 'Knowledge of any contraceptive method'}, inplace=True)

# v302a # Ever used anything or tried to delay or avoid getting pregnant # 1508 NaN
IR_sex['v302a'].replace('no', 0, inplace=True)
IR_sex['v302a'].replace('yes, used outside calendar', 1, inplace=True)
IR_sex['v302a'].replace('yes, used in calendar', 2, inplace=True)
IR_sex.rename(columns={'v302a': 'Ever used anything or tried to delay or avoid getting pregnant'}, inplace=True)

# v312 # Current contraceptive method # condom & male condom & female condom: 1 - other: 0 # dummy # 1508 NaN
IR_sex['v312'].replace(float('NaN'), -1, inplace=True)
searchfor = ['condom', 'male condom', 'female condom', -1]
IR_sex['v312'].replace(list(set(list(IR_sex['v312'].unique()))-set(searchfor)), 0, inplace=True)
IR_sex['v312'].replace(('condom', 'male condom', 'female condom'), 1, inplace=True)
IR_sex['v312'].replace(-1, float('NaN'), inplace=True)
IR_sex.rename(columns={'v312': 'Current contraceptive method'}, inplace=True)

# v313 # Current contraceptive by method type # ordinal # 0 to 3 # 1508 NaN
IR_sex['v313'].replace('no method', 0, inplace=True)
IR_sex['v313'].replace('folkloric method', 1, inplace=True)
IR_sex['v313'].replace('traditional method', 2, inplace=True)
IR_sex['v313'].replace('modern method', 3, inplace=True)
IR_sex.rename(columns={'v313': 'Current contraceptive by method type'}, inplace=True)

# v361 # Pattern of contraceptive use # 7203 NaN
IR_sex['v361'].replace('never used', 0, inplace=True)
IR_sex['v361'].replace('used before last birth', 1, inplace=True)
IR_sex['v361'].replace('used since last birth', 2, inplace=True)
IR_sex['v361'].replace('currently using', 3, inplace=True)
IR_sex.rename(columns={'v361': 'Pattern of contraceptive use'}, inplace=True)

# v364 # Contraceptive use and intention # 7203 NaN
IR_sex['v364'].replace('does not intend to use', 0, inplace=True)
IR_sex['v364'].replace('non-user - intends to use later', 1, inplace=True)
IR_sex['v364'].replace('using traditional method', 2, inplace=True)
IR_sex['v364'].replace('using modern method', 3, inplace=True)
IR_sex.rename(columns={'v364': 'Contraceptive use and intention'}, inplace=True)

# v384a # Heard family planning on radio last few months # dummy # 7203 NaN
IR_sex['v384a'].replace('no', 0, inplace=True)
IR_sex['v384a'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v384a': 'Heard family planning on radio last few months'}, inplace=True)

# v384b # Heard family planning on TV last few months # dummy # 7204 NaN
IR_sex['v384b'].replace('no', 0, inplace=True)
IR_sex['v384b'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v384b': 'Heard family planning on TV last few months'}, inplace=True)

# v384c # Heard family planning in newspaper/magazine last few months # dummy # 7238 NaN
IR_sex['v384c'].replace('no', 0, inplace=True)
IR_sex['v384c'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v384c': 'Heard family planning in newspaper/magazine last few months'}, inplace=True)

# v393 # Visited by fieldworker in last 12 months # 13247 NaN
IR_sex['v393'].replace('no', 0, inplace=True)
IR_sex['v393'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v393': 'Visited by fieldworker in last 12 months'}, inplace=True)

# v394 # Visited health facility last 12 months # 7219 NaN
IR_sex['v394'].replace('no', 0, inplace=True)
IR_sex['v394'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v394': 'Visited health facility last 12 months'}, inplace=True)

# v404 # Currently breastfeeding # 777 NaN
IR_sex['v404'].replace('no', 0, inplace=True)
IR_sex['v404'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v404': 'Currently breastfeeding'}, inplace=True)

# v405 # Currently amenorrheic # 6472 NaN
IR_sex['v405'].replace('no', 0, inplace=True)
IR_sex['v405'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v405': 'Currently amenorrheic'}, inplace=True)

# v406 # Currently abstaining # 6472 NaN
IR_sex['v406'].replace('no', 0, inplace=True)
IR_sex['v406'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v406': 'Currently abstaining'}, inplace=True)

# v416 # Heard of oral rehydration # 6589 NaN
IR_sex['v416'].replace('never heard of', 0, inplace=True)
IR_sex['v416'].replace('heard of ors', 1, inplace=True)
IR_sex['v416'].replace('used ors', 2, inplace=True)
IR_sex.rename(columns={'v416': 'Heard of oral rehydration'}, inplace=True)

# v417 # Entries in pregnancy and postnatal care roster
IR_sex.rename(columns={'v417': 'Entries in pregnancy and postnatal care roster'}, inplace=True)

# v418 # Entries in immunization roster
IR_sex.rename(columns={'v418': 'Entries in immunization roster'}, inplace=True)

# v446 # Rohrer's index # 17563 NaN
IR_sex['v446'].replace('flagged cases', float('NaN'), inplace=True)
IR_sex.rename(columns={'v446': 'Rohrer\'s index'}, inplace=True)

# v459 # Have mosquito bed net for sleeping # 14079 NaN
IR_sex['v459'].replace('no', 0, inplace=True)
IR_sex['v459'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v459': 'Have mosquito bed net for sleeping'}, inplace=True)

# v461 # Respondent slept under mosquito bed net # 14079 NaN
IR_sex['v461'].replace('no', 0, inplace=True)
IR_sex['v461'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v461': 'Respondent slept under mosquito bed net'}, inplace=True)

# v463z # Does not use cigarettes and tobacco # 6472 NaN
IR_sex['v463z'].replace('no', 0, inplace=True)
IR_sex['v463z'].replace('yes, smokes nothing', 1, inplace=True)
IR_sex.rename(columns={'v463z': 'Does not use cigarettes and tobacco'}, inplace=True)

# v467b # Getting medical help for self: getting permission to go # 6488 NaN
IR_sex['v467b'].replace('not a big problem', 0, inplace=True)
IR_sex['v467b'].replace('big problem', 1, inplace=True)
IR_sex.rename(columns={'v467b': 'Getting medical help for self: getting permission to go'}, inplace=True)

# v467c # Getting medical help for self: getting money needed for treatment # 6489 NaN
IR_sex['v467c'].replace('not a big problem', 0, inplace=True)
IR_sex['v467c'].replace('big problem', 1, inplace=True)
IR_sex.rename(columns={'v467c': 'Getting medical help for self: getting money needed for treatment'}, inplace=True)

# v467d # Getting medical help for self: distance to health facility # 6487 NaN
IR_sex['v467d'].replace('not a big problem', 0, inplace=True)
IR_sex['v467d'].replace('big problem', 1, inplace=True)
IR_sex.rename(columns={'v467d': 'Getting medical help for self: distance to health facility'}, inplace=True)

# # v467f # Getting medical help for self: not wanting to go alone # 6493 NaN
IR_sex['v467f'].replace('not a big problem', 0, inplace=True)
IR_sex['v467f'].replace('big problem', 1, inplace=True)
IR_sex.rename(columns={'v467f': 'Getting medical help for self: not wanting to go alone'}, inplace=True)

# v468 # Record for Last Birth # dummy # 75 NaN
IR_sex['v468'].replace('last birth only', 0, inplace=True)
IR_sex['v468'].replace('all births in last 5 years', 1, inplace=True)
IR_sex.rename(columns={'v468': 'Record for Last Birth'}, inplace=True)

# v477 # Number of injections in last 12 months # 10 NaN
IR_sex['v477'].replace('none', 0, inplace=True)
IR_sex['v477'].replace(('don\'t know'), float('NaN'), inplace=True)
IR_sex['v477'].replace('90+', 146, inplace=True)
IR_sex.rename(columns={'v477': 'Number of injections in last 12 months'}, inplace=True)

# v481 # Covered by health insurance # 4957 NaN
IR_sex['v481'].replace('no', 0, inplace=True)
IR_sex['v481'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v481': 'Covered by health insurance'}, inplace=True)

# v502 # Currently/formerly/never in union # OneHot encoding
IR_sex['v502'].replace('never in union', 0, inplace=True)
IR_sex['v502'].replace('formerly in union/living with a man', 1, inplace=True)
IR_sex['v502'].replace('currently in union/living with a man', 2, inplace=True)
IR_sex.rename(columns={'v502': 'Currently/formerly/never in union'}, inplace=True)

# v503 # Number of unions # 10512 NaN
IR_sex['v503'].replace('once', 0, inplace=True)
IR_sex['v503'].replace('more than once', 1, inplace=True)
IR_sex.rename(columns={'v503': 'Number of unions'}, inplace=True)

# v511 # Age at first cohabitation # 10471 NaN
IR_sex.rename(columns={'v511': 'Age at first cohabitation'}, inplace=True)

# v512 # Years since first cohabitation # 10471 NaN
IR_sex.rename(columns={'v512': 'Years since first cohabitation'}, inplace=True)

# v513 # Cohabitation duration (grouped)
IR_sex['v513'].replace('never married', 0, inplace=True)
IR_sex['v513'].replace('0-4', 1, inplace=True)
IR_sex['v513'].replace('5-9', 2, inplace=True)
IR_sex['v513'].replace('10-14', 3, inplace=True)
IR_sex['v513'].replace('15-19', 4, inplace=True)
IR_sex['v513'].replace('20-24', 5, inplace=True)
IR_sex['v513'].replace('25-29', 6, inplace=True)
IR_sex['v513'].replace('30+', 7, inplace=True)
IR_sex.rename(columns={'v513': 'Cohabitation duration (grouped)'}, inplace=True)

# v527 # Time since last sex (in days) # 31 NaN
IR_sex['v527'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v527'].replace('<1 day ago', 100, inplace=True)
IR_sex['v527'].replace('days: 1', 101, inplace=True)
IR_sex['v527'].replace('weeks: 1', 201, inplace=True)
IR_sex['v527'].replace('months: 1', 301, inplace=True)
IR_sex['v527'].replace('years: 1', 401, inplace=True)
IR_sex['v527'].replace('years: number missing', np.mean(IR_sex.loc[IR_sex.v527.astype('str').str.match('^4'), 'v527']), inplace=True)
flag_1 = IR_sex.v527.astype('str').str.match('^1')
flag_2 = IR_sex.v527.astype('str').str.match('^2')
flag_3 = IR_sex.v527.astype('str').str.match('^3')
flag_4 = IR_sex.v527.astype('str').str.match('^4')
IR_sex.loc[flag_1, 'v527'] = IR_sex.loc[flag_1, 'v527'] - 100
IR_sex.loc[flag_2, 'v527'] = (IR_sex.loc[flag_2, 'v527'] - 200) * 7
IR_sex.loc[flag_3, 'v527'] = (IR_sex.loc[flag_3, 'v527'] - 300) * 30
IR_sex.loc[flag_4, 'v527'] = round((IR_sex.loc[flag_4, 'v527'] - 400) * 365)
IR_sex.rename(columns={'v527': 'Time since last sex (in days)'}, inplace=True)

# v531 # Age at first sex (imputed) # 2494 NaN
IR_sex.rename(columns={'v531': 'Age at first sex (imputed)'}, inplace=True)

# v536 # Recent sexual activity # 1594 NaN
IR_sex['v536'].replace(('not active in last 4 weeks - not postpartum abstinence', 'not active in last 4 weeks - postpartum abstinence'), 0, inplace=True)
IR_sex['v536'].replace('active in last 4 weeks', 1, inplace=True)
IR_sex.rename(columns={'v536': 'Recent sexual activity'}, inplace=True)

# v602 # Fertility preference # 1594 NaN
IR_sex['v602'].replace(('sterilized (respondent or partner)', 'declared infecund'), 0, inplace=True)
IR_sex['v602'].replace('no more', 1, inplace=True)
IR_sex['v602'].replace('undecided', 2, inplace=True)
IR_sex['v602'].replace('have another', 3, inplace=True)
IR_sex.rename(columns={'v602': 'Fertility preference'}, inplace=True)

# v613 # Ideal number of children # 7207 NaN
IR_sex['v613'].replace(('non-numeric response', 'god\'s will', 'don\'t know'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v613': 'Ideal number of children'}, inplace=True)

# v625a # Fecund (definition 3) # 1508 NaN
IR_sex['v625a'].replace(('infecund, menopausal', 'pregnant', 'postpartum amenorrheic'), 0, inplace=True)
IR_sex['v625a'].replace('fecund', 1, inplace=True)
IR_sex.rename(columns={'v625a': 'Fecund (definition 3)'}, inplace=True)

# v626a # Unmet need for contraception # 1533 NaN
IR_sex = pd.get_dummies(IR_sex, columns=['v626a'], prefix=['Unmet need for contraception'], dummy_na=True)
for indice in list(IR_sex['Unmet need for contraception_nan'].loc[IR_sex['Unmet need for contraception_nan']==1].index):
    for colonne in list(IR_sex.columns):
        if re.match('Unmet need for contraception', colonne):
            IR_sex.loc[indice, colonne] = float('NaN')
IR_sex.drop('Unmet need for contraception_nan', axis='columns', inplace=True)

# v627 # Ideal number of boys # 7209 NaN
IR_sex['v627'].replace(('other', 'god\'s will', 'don\'t know'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v627': 'Ideal number of boys'}, inplace=True)

# v628 # Ideal number of girls # 7209 NaN
IR_sex['v628'].replace(('other', 'god\'s will', 'don\'t know'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v628': 'Ideal number of girls'}, inplace=True)

# v629 # Ideal number of either sex # 7209 NaN
IR_sex['v629'].replace(('other', 'god\'s will', 'don\'t know'), float('NaN'), inplace=True)
IR_sex.rename(columns={'v629': 'Ideal number of either sex'}, inplace=True)

# v633b # Wife justified refusing sex: husband has other women # 781 NaN
IR_sex['v633b'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v633b'].replace('no', 0, inplace=True)
IR_sex['v633b'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v633b': 'Wife justified refusing sex: husband has other women'}, inplace=True)

# v714 # Currently working # 91 NaN
IR_sex['v714'].replace('no', 0, inplace=True)
IR_sex['v714'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v714': 'Currently working'}, inplace=True)

# v717 # Occupation (grouped) # nominal # OneHot encoding # 1555 NaN
IR_sex['v717'].replace(('others', 'armed forces'), 'other', inplace=True)
IR_sex['v717'].replace('did not work', 'not working', inplace=True)
IR_sex['v717'].replace(('agricultural - self employed', 'agricultural - employee'), 'agricultural', inplace=True)
IR_sex['v717'].replace(('unskilled manual', 'skilled manual'), 'manual', inplace=True)
IR_sex['v717'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex = pd.get_dummies(IR_sex, columns=['v717'], prefix=['Occupation'], dummy_na=True)
for indice in list(IR_sex['Occupation_nan'].loc[IR_sex['Occupation_nan']==1].index):
    for colonne in list(IR_sex.columns):
        if re.match('Occupation', colonne):
            IR_sex.loc[indice, colonne] = float('NaN')
IR_sex.drop('Occupation_nan', axis='columns', inplace=True)

# v731 # Respondent worked in last 7 days # ordinal # 3 NaN
IR_sex['v731'].replace('no', 0, inplace=True)
IR_sex['v731'].replace('in the past year', 1, inplace=True)
IR_sex['v731'].replace('have a job, but on leave last 7 days', 2, inplace=True)
IR_sex['v731'].replace('currently working', 3, inplace=True)
IR_sex.rename(columns={'v731': 'Respondent worked in last 7 days'}, inplace=True)

# v744 # Beating justified # ordinal (constructed) # 4, 7, 25, 12, 8 NaN
IR_sex['v744a'].replace('no', 0, inplace=True)
IR_sex['v744b'].replace('no', 0, inplace=True)
IR_sex['v744c'].replace('no', 0, inplace=True)
IR_sex['v744d'].replace('no', 0, inplace=True)
IR_sex['v744e'].replace('no', 0, inplace=True)
IR_sex['v744a'].replace(('don\'t know', 'yes'), 1, inplace=True)
IR_sex['v744b'].replace(('don\'t know', 'yes'), 1, inplace=True)
IR_sex['v744c'].replace(('don\'t know', 'yes'), 1, inplace=True)
IR_sex['v744d'].replace(('don\'t know', 'yes'), 1, inplace=True)
IR_sex['v744e'].replace(('don\'t know', 'yes'), 1, inplace=True)
IR_sex['v744a'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v744b'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v744c'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v744d'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v744e'].replace(float('NaN'), 0.1, inplace=True)
IR_sex.loc[:, 'v744'] = round(IR_sex.loc[:, 'v744a'] + IR_sex.loc[:, 'v744b'] + IR_sex.loc[:, 'v744c'] + IR_sex.loc[:, 'v744d'] + IR_sex.loc[:, 'v744e'], 1)
IR_sex.loc[:, 'v744'] = np.where((IR_sex.loc[:, 'v744']==0.3) , float('NaN'), IR_sex.loc[:, 'v744'])
IR_sex.loc[:, 'v744'] = np.where((IR_sex.loc[:, 'v744']==0.4) , float('NaN'), IR_sex.loc[:, 'v744'])
IR_sex.loc[:, 'v744'] = np.where((IR_sex.loc[:, 'v744']==0.5) , float('NaN'), IR_sex.loc[:, 'v744'])
IR_sex.loc[:, 'v744'] = np.where((IR_sex.loc[:, 'v744']>=1), 1, IR_sex.loc[:, 'v744'])
IR_sex.loc[:, 'v744'] = np.where((IR_sex.loc[:, 'v744']<=0.2), 0, IR_sex.loc[:, 'v744'])
IR_sex.rename(columns={'v744': 'Beating justified'}, inplace=True)
IR_sex.drop(['v744a', 'v744b', 'v744c', 'v744d', 'v744e'], axis='columns', inplace=True)

# v745a # Owns a house alone or jointly # 8057 NaN
IR_sex['v745a'].replace('does not own', 0, inplace=True)
IR_sex['v745a'].replace('jointly only', 1, inplace=True)
IR_sex['v745a'].replace('alone only', 2, inplace=True)
IR_sex['v745a'].replace('both alone and jointly', 3, inplace=True)
IR_sex.rename(columns={'v745a': 'Owns a house alone or jointly'}, inplace=True)

# v745b # Owns land alone or jointly # 8057 NaN
IR_sex['v745b'].replace('does not own', 0, inplace=True)
IR_sex['v745b'].replace('jointly only', 1, inplace=True)
IR_sex['v745b'].replace('alone only', 2, inplace=True)
IR_sex['v745b'].replace('both alone and jointly', 3, inplace=True)
IR_sex.rename(columns={'v745b': 'Owns land alone or jointly'}, inplace=True)

# v750 # Ever heard of a Sexually Transmitted Infection (STI) # dummy # 3 NaN
IR_sex['v750'].replace('no', 0, inplace=True)
IR_sex['v750'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v750': 'Ever heard of a Sexually Transmitted Infection (STI)'}, inplace=True)

# v751 # Ever heard of AIDS # dummy
IR_sex['v751'].replace('no', 0, inplace=True)
IR_sex['v751'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v751': 'Ever heard of AIDS'}, inplace=True)

# v754 # Reduce risk of getting HIV # ordinal (constructed)
IR_sex['v754cp'].replace(('don\'t know', 'no'), 1, inplace=True)
IR_sex['v754cp'].replace('yes', 0, inplace=True)
IR_sex['v754dp'].replace(('don\'t know', 'no'), 1, inplace=True)
IR_sex['v754dp'].replace('yes', 0, inplace=True)
IR_sex['v756'].replace(('don\'t know', 'no'), 1, inplace=True)
IR_sex['v756'].replace('yes', 0, inplace=True)
IR_sex['v754jp'].replace(('don\'t know', 'yes'), 1, inplace=True)
IR_sex['v754jp'].replace('no', 0, inplace=True)
IR_sex['v754wp'].replace(('don\'t know', 'yes'), 1, inplace=True)
IR_sex['v754wp'].replace('no', 0, inplace=True)
IR_sex['v823'].replace(('don\'t know', 'yes'), 1, inplace=True)
IR_sex['v823'].replace('no', 0, inplace=True)
IR_sex['v754cp'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v754dp'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v756'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v754jp'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v754wp'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v823'].replace(float('NaN'), 0.1, inplace=True)
IR_sex.loc[:, 'v754'] = round(IR_sex.loc[:, 'v754cp'] + IR_sex.loc[:, 'v754dp'] + IR_sex.loc[:, 'v754jp'] + IR_sex.loc[:, 'v754wp'] + IR_sex.loc[:, 'v756'] + IR_sex.loc[:, 'v823'], 1)
IR_sex.loc[:, 'v754'] = np.where((IR_sex.loc[:, 'v754']==0.3) , float('NaN'), IR_sex.loc[:, 'v754'])
IR_sex.loc[:, 'v754'] = np.where((IR_sex.loc[:, 'v754']==0.4) , float('NaN'), IR_sex.loc[:, 'v754'])
IR_sex.loc[:, 'v754'] = np.where((IR_sex.loc[:, 'v754']==0.5) , float('NaN'), IR_sex.loc[:, 'v754'])
IR_sex.loc[:, 'v754'] = np.where((IR_sex.loc[:, 'v754']==0.6) , float('NaN'), IR_sex.loc[:, 'v754'])
IR_sex.loc[:, 'v754'] = np.where((IR_sex.loc[:, 'v754']>=1), 1, IR_sex.loc[:, 'v754'])
IR_sex.loc[:, 'v754'] = np.where((IR_sex.loc[:, 'v754']<=0.2), 0, IR_sex.loc[:, 'v754'])
IR_sex.rename(columns={'v754': 'Reduce risk of getting HIV'}, inplace=True)
IR_sex.drop(['v754cp', 'v754dp', 'v754jp', 'v754wp', 'v756', 'v823'], axis='columns', inplace=True)

# v761 # Condom used during last sex with most recent partner # dummy # 10778 NaN
IR_sex['v761'].replace('no', 0, inplace=True)
IR_sex['v761'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v761': 'Condom used during last sex with most recent partner'}, inplace=True)

# v763a # Had any STI in last 12 months # dummy # 137 NaN
IR_sex['v763a'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v763a'].replace('no', 0, inplace=True)
IR_sex['v763a'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v763a': 'Had any STI in last 12 months'}, inplace=True)

# v763b # Had genital sore/ulcer in last 12 months # dummy # 73 NaN
IR_sex['v763b'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v763b'].replace('no', 0, inplace=True)
IR_sex['v763b'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v763b': 'Had genital sore/ulcer in last 12 months'}, inplace=True)

# v763c # Had genital discharge in last 12 months # dummy # 66 NaN
IR_sex['v763c'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v763c'].replace('no', 0, inplace=True)
IR_sex['v763c'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v763c': 'Had genital discharge in last 12 months'}, inplace=True)

# v766b # Number of sex partners, including spouse, in last 12 months # 88 NaN
IR_sex['v766b'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex.rename(columns={'v766b': 'Number of sex partners, including spouse, in last 12 months'}, inplace=True)

# v767a # Relationship with most recent sex partner # 10779 NaN
IR_sex['v767a'].replace('spouse', 0, inplace=True)
IR_sex['v767a'].replace('live-in partner', 1, inplace=True)
IR_sex['v767a'].replace('boyfriend not living with respondent', 2, inplace=True)
IR_sex['v767a'].replace('casual acquaintance', 3, inplace=True)
IR_sex['v767a'].replace('other', 4, inplace=True)
IR_sex['v767a'].replace('commercial sex worker', 5, inplace=True)
IR_sex.rename(columns={'v767a': 'Relationship with most recent sex partner'}, inplace=True)

# v774 # Ways of transmission from mother to child # dummy # 2667 - 2663 - 2663 NaN
IR_sex['v774a'].replace(('don\'t know', 'no'), 1, inplace=True)
IR_sex['v774a'].replace('yes', 0, inplace=True)
IR_sex['v774b'].replace(('don\'t know', 'no'), 1, inplace=True)
IR_sex['v774b'].replace('yes', 0, inplace=True)
IR_sex['v774c'].replace(('don\'t know', 'no'), 1, inplace=True)
IR_sex['v774c'].replace('yes', 0, inplace=True)
IR_sex['v774a'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v774b'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v774c'].replace(float('NaN'), 0.1, inplace=True)
IR_sex.loc[:, 'v774'] = round(IR_sex.loc[:, 'v774a'] + IR_sex.loc[:, 'v774b'] + IR_sex.loc[:, 'v774c'], 1)
IR_sex.loc[:, 'v774'] = np.where((IR_sex.loc[:, 'v774']==0.2) , float('NaN'), IR_sex.loc[:, 'v774'])
IR_sex.loc[:, 'v774'] = np.where((IR_sex.loc[:, 'v774']==0.3) , float('NaN'), IR_sex.loc[:, 'v774'])
IR_sex.loc[:, 'v774'] = np.where((IR_sex.loc[:, 'v774']>=1), 1, IR_sex.loc[:, 'v774'])
IR_sex.loc[:, 'v774'] = np.where((IR_sex.loc[:, 'v774']<=0.1), 0, IR_sex.loc[:, 'v774'])
IR_sex.rename(columns={'v774': 'Ways of transmission from mother to child'}, inplace=True)
IR_sex.drop(['v774a', 'v774b', 'v774c'], axis='columns', inplace=True)

# v781 # Ever been tested for HIV # dummy # 75 NaN
IR_sex['v781'].replace('no', 0, inplace=True)
IR_sex['v781'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v781': 'Ever been tested for HIV'}, inplace=True)

# v783 # Know a place to get HIV test # 2746 NaN
IR_sex['v783'].replace('no', 0, inplace=True)
IR_sex['v783'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v783': 'Know a place to get HIV test'}, inplace=True)

# v785 # Heard about other STIs # 88 NaN
IR_sex['v785'].replace('no', 0, inplace=True)
IR_sex['v785'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v785': 'Heard about other STIs'}, inplace=True)

# v811 - v812 - v813 - v814 # Presence of other people for \'Wife beating justified\' questions # 16 NaN
IR_sex['v811'].replace('no', 0, inplace=True)
IR_sex['v812'].replace('no', 0, inplace=True)
IR_sex['v813'].replace('no', 0, inplace=True)
IR_sex['v814'].replace('no', 0, inplace=True)
IR_sex['v811'].replace(('yes - not listening', 'yes - listening'), 1, inplace=True)
IR_sex['v812'].replace(('yes - not listening', 'yes - listening'), 1, inplace=True)
IR_sex['v813'].replace(('yes - not listening', 'yes - listening'), 1, inplace=True)
IR_sex['v814'].replace(('yes - not listening', 'yes - listening'), 1, inplace=True)
IR_sex['v811'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v812'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v813'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v814'].replace(float('NaN'), 0.1, inplace=True)
tmp_col_name = 'Presence of other people for \'Wife beating justified\' questions'
IR_sex.loc[:, tmp_col_name] = round(IR_sex.loc[:, 'v811'] + IR_sex.loc[:, 'v812'] + IR_sex.loc[:, 'v813'] + IR_sex.loc[:, 'v814'], 1)
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]==0.2), float('NaN'), IR_sex.loc[:, tmp_col_name])
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]==0.3), float('NaN'), IR_sex.loc[:, tmp_col_name])
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]==0.4), float('NaN'), IR_sex.loc[:, tmp_col_name])
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]>=1), 1, IR_sex.loc[:, tmp_col_name])
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]<=0.1), 0, IR_sex.loc[:, tmp_col_name])
IR_sex.drop(['v811', 'v812', 'v813', 'v814'], axis='columns', inplace=True)

# v815a - v815b - v815c # Presence of other people during the sexual activity section of the interview # 23 - 24 - 31 NaN
IR_sex['v815a'].replace('no', 0, inplace=True)
IR_sex['v815b'].replace('no', 0, inplace=True)
IR_sex['v815c'].replace('no', 0, inplace=True)
IR_sex['v815a'].replace('yes', 1, inplace=True)
IR_sex['v815b'].replace('yes', 1, inplace=True)
IR_sex['v815c'].replace('yes', 1, inplace=True)
IR_sex['v815a'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v815b'].replace(float('NaN'), 0.1, inplace=True)
IR_sex['v815c'].replace(float('NaN'), 0.1, inplace=True)
tmp_col_name = 'Presence of other people during the sexual activity section of the interview'
IR_sex.loc[:, tmp_col_name] = round(IR_sex.loc[:, 'v815a'] + IR_sex.loc[:, 'v815b'] + IR_sex.loc[:, 'v815c'], 1)
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]==0.2), float('NaN'), IR_sex.loc[:, tmp_col_name])
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]==0.3), float('NaN'), IR_sex.loc[:, tmp_col_name])
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]>=1), 1, IR_sex.loc[:, tmp_col_name])
IR_sex.loc[:, tmp_col_name] = np.where((IR_sex.loc[:, tmp_col_name]<=0.1), 0, IR_sex.loc[:, tmp_col_name])
IR_sex.drop(['v815a', 'v815b', 'v815c'], axis='columns', inplace=True)

# v822 # Wife justified asking husband to use condom if he has STI # 11 NaN
IR_sex['v822'].replace(('don\'t know', 'no'), 0, inplace=True)
IR_sex['v822'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v822': 'Wife justified asking husband to use condom if he has STI'}, inplace=True)

# v824 # Drugs to avoid HIV transmission to baby during pregnancy # 7628 NaN
IR_sex['v824'].replace(('don\'t know', 'no'), 0, inplace=True)
IR_sex['v824'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v824': 'Drugs to avoid HIV transmission to baby during pregnancy'}, inplace=True)

# v825 # Would buy vegetables from vendor with HIV # 2677 NaN
IR_sex['v825'].replace(('don\'t know', 'no'), 0, inplace=True)
IR_sex['v825'].replace('yes', 1, inplace=True)
IR_sex.rename(columns={'v825': 'Would buy vegetables from vendor with HIV'}, inplace=True)

# v834a # Age of most recent partner # 10856 NaN
IR_sex['v834a'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v834a'].replace('95+', 100, inplace=True)
IR_sex.rename(columns={'v834a': 'Age of most recent partner'}, inplace=True)

# v836 # Total lifetime number of sex partners # 30 NaN
IR_sex['v836'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v836'].replace('95+', 146, inplace=True)
IR_sex.rename(columns={'v836': 'Total lifetime number of sex partners'}, inplace=True)

# v853a # Times in last 12 months had sex with most recent partner # 10896 NaN
IR_sex['v853a'].replace('don\'t know', float('NaN'), inplace=True)
IR_sex['v853a'].replace('95+', 146, inplace=True)
IR_sex.rename(columns={'v853a': 'Times in last 12 months had sex with most recent partner'}, inplace=True)

# LATNUM # 
IR_sex.rename(columns={'LATNUM': 'Cluster\'s latitude coordinate'}, inplace=True)

# LONGNUM # Current age
IR_sex.rename(columns={'LONGNUM': 'Cluster\'s longitude coordinate'}, inplace=True)

# drop uninformative features
IR_drop_list = ['caseid', 'v000', 'v001', 'v002', 'v003', 'v005', 'v006', 'v007', 'v008', 'v009', 'v010', 'v011', 'v013', 'v014',
                'v016', 'v017', 'v018', 'v019', 'v022', 'v024', 'v027', 'v028', 'v030', 'v042', 'v044', 'v107', 'v116', 'v127',
                'v128', 'v139', 'v140', 'v149', 'v161', 'v201', 'v215', 'v218', 'v219', 'v220', 'v226', 'v227', 'v304a_17', 'v304a_18',
                'v304_01', 'v304_02', 'v304_03', 'v304_04', 'v304_05', 'v304_06', 'v304_07', 'v304_08', 'v304_09', 'v304_10',
                'v304_11', 'v304_13', 'v304_14', 'v304_16', 'v304_17', 'v304_18', 'v307_01', 'v307_03', 'v307_05', 'v307_11', 'v437',
                'v438', 'v439', 'v445', 'v440', 'v441', 'v442', 'v443', 'v444', 'v444a', 'v447', 'v447a', 'v463a', 'v463b', 'v463c', 'v463x', 
                'v473a', 'v481b', 'v481d', 'v481x', 'v501', 'v507', 'v508', 'v509', 'v510', 'v525', 'v528', 'v529', 'v530', 'v532', 'v605', 
                'v614', 'v623', 'v624', 'v625', 'v626', 'v766a', 'v801', 'v802', 'v803', 'vcal_1', 'vcal_2', 'hiv01', 'hiv02', 'hiv05',
                'DHSID', 'DHSYEAR', 'CCFIPS', 'ADM1FIPS', 'ADM1FIPSNA', 'ADM1SALBNA', 'ADM1SALBCO', 'ADM1DHS', 'ADM1NAME',
                'DHSREGCO', 'DHSREGNA', 'SOURCE', 'URBAN_RURA', 'ALT_GPS', 'ALT_DEM']
IR_sex = IR_sex.drop(IR_drop_list, axis='columns')

# pickling transformed data
f = open('data/processed/IR_features.pkl', 'wb')
pickle.dump(IR_sex, f)
f.close()