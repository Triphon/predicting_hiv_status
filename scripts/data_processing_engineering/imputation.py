# import libraries
import os
import re
import pandas as pd
import numpy as np
import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import VarianceThreshold
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer

# specifying constant parameters
random_seed = 5

# set working directory
working_directory = '/home/oreler/predicting_hiv_status'
os.chdir(working_directory)

# upload pickled data
MR = pickle.load(open('data/processed/MR_features.pkl', 'rb'))
IR = pickle.load(open('data/processed/IR_features.pkl', 'rb'))

# Country # nominal # OneHot encoding
MR_country_dummy = pd.get_dummies(MR, columns=['country'], prefix='Country')
IR_country_dummy = pd.get_dummies(IR, columns=['country'], prefix='Country')

# Rename countries
country_list = ['Angola', 'Burundi', 'Ethiopia', 'Lesotho', 'Malawi', 'Mozambique', 'Namibia', 'Rwanda', 'Zambia', 'Zimbabwe']
i = 0
for colonne in list(MR_country_dummy.columns):
    if re.match('Country', colonne):
        MR_country_dummy.rename(columns={colonne: country_list[i]}, inplace=True)
        i += 1
i = 0
for colonne in list(IR_country_dummy.columns):
    if re.match('Country', colonne):
        IR_country_dummy.rename(columns={colonne: country_list[i]}, inplace=True)
        i += 1

# extraction of column names
MR_col_names = MR_country_dummy.columns.drop('hiv03')
IR_col_names = IR_country_dummy.columns.drop('hiv03')

# # remove variables with very low variance (treshold ?!?)
# constant_filter = VarianceThreshold(threshold=0.01)
# constant_filter.fit(MR)
# len(MR.columns[constant_filter.get_support()])
# constant_columns = [column for column in MR.columns if column not in MR.columns[constant_filter.get_support()]]
# MR.drop(labels=constant_columns, axis=1, inplace=True)
# constant_filter.fit(IR)
# len(IR.columns[constant_filter.get_support()])
# constant_columns = [column for column in IR.columns if column not in IR.columns[constant_filter.get_support()]]
# IR.drop(labels=constant_columns, axis=1, inplace=True)

# # correlation matrix
# MR_correlated_features = set()
# IR_correlated_features = set()
# MR_correlation_matrix = MR.corr()
# IR_correlation_matrix = IR.corr()
    
# # remove columns for correlation above 0.8
# for i in range(len(CMR_correlation_matrix.columns)):
#     for j in range(i):
#         if abs(CMR_correlation_matrix.iloc[i, j]) > 0.8:
#             colname = CMR_correlation_matrix.columns[i]
#             CMR_correlated_features.add(colname)
# len(CMR_correlated_features)
# CMR.drop(labels=CMR_correlated_features, axis=1, inplace=True)

# for i in range(len(CIR_correlation_matrix.columns)):
#     for j in range(i):
#         if abs(CIR_correlation_matrix.iloc[i, j]) > 0.8:
#             colname = CIR_correlation_matrix.columns[i]
#             CIR_correlated_features.add(colname)
# len(CIR_correlated_features)
# CIR.drop(labels=CIR_correlated_features, axis=1, inplace=True)

#===============================================================================================================================
# subsampling 9 countries out of 10
#===============================================================================================================================

CMR_train = {}
CMR_test = {}
CIR_train = {}
CIR_test = {}
CMR_Y_train = {}
CMR_Y_test = {}
CIR_Y_train = {}
CIR_Y_test = {}

for name in list(country_list):
    CMR_train[name] = MR_country_dummy[MR_country_dummy[name]!=1].astype('float')
    CMR_test[name] = MR_country_dummy[MR_country_dummy[name]==1].astype('float')
    CIR_train[name] = IR_country_dummy[IR_country_dummy[name]!=1].astype('float')
    CIR_test[name] = IR_country_dummy[IR_country_dummy[name]==1].astype('float')
    CMR_Y_train[name] = MR_country_dummy['hiv03'][MR_country_dummy[name]!=1].astype('float')
    CMR_Y_test[name] = MR_country_dummy['hiv03'][MR_country_dummy[name]==1].astype('float')
    CIR_Y_train[name] = IR_country_dummy['hiv03'][IR_country_dummy[name]!=1].astype('float')
    CIR_Y_test[name] = IR_country_dummy['hiv03'][IR_country_dummy[name]==1].astype('float')
    CMR_train[name].drop(columns='hiv03', inplace=True)
    CMR_test[name].drop(columns='hiv03', inplace=True)
    CIR_train[name].drop(columns='hiv03', inplace=True)
    CIR_test[name].drop(columns='hiv03', inplace=True)

#================================================================================================================================
# remove country flag - impute - standardize
#================================================================================================================================

MR_X_train = {}
MR_X_test = {}
MR_Y_train = {}
MR_Y_test = {}
IR_X_train = {}
IR_X_test = {}
IR_Y_train = {}
IR_Y_test = {}

# split between train (80%) and test (20%) with stratification
for name in list(country_list):  
    MR_X_train[name], MR_X_test[name], MR_Y_train[name], MR_Y_test[name] = train_test_split(CMR_train[name], CMR_Y_train[name], test_size=0.2, stratify=CMR_Y_train[name], random_state=random_seed)
    IR_X_train[name], IR_X_test[name], IR_Y_train[name], IR_Y_test[name] = train_test_split(CIR_train[name], CIR_Y_train[name], test_size=0.2, stratify=CIR_Y_train[name], random_state=random_seed)

# multiple imputations using chained equations
n_imputations = 5
MR_X_train_comp = {}
MR_X_test_comp = {}
CMR_test_comp = {}
IR_X_train_comp = {}
IR_X_test_comp = {}
CIR_test_comp = {}
MR_X_train_imp = {}
MR_X_test_imp = {}
CMR_test_imp = {}
IR_X_train_imp = {}
IR_X_test_imp = {}
CIR_test_imp = {}

for name in list(country_list):
    MR_X_train_comp[name] = []
    MR_X_test_comp[name] = []
    CMR_test_comp[name] = []
    IR_X_train_comp[name] = []
    IR_X_test_comp[name] = []
    CIR_test_comp[name] = []
    for i in range(n_imputations):
        print(f'Current imputed country is {name}')
        print(f'Current imputation round is {i+1}/{n_imputations}')
        MR_imputer = IterativeImputer(sample_posterior=True, random_state=i, verbose=1)
        MR_X_train_comp[name].append(MR_imputer.fit_transform(MR_X_train[name]))
        MR_X_test_comp[name].append(MR_imputer.transform(MR_X_test[name]))
        CMR_test_comp[name].append(MR_imputer.transform(CMR_test[name]))
        IR_imputer = IterativeImputer(sample_posterior=True, random_state=i, verbose=1)
        IR_X_train_comp[name].append(IR_imputer.fit_transform(IR_X_train[name]))
        IR_X_test_comp[name].append(IR_imputer.transform(IR_X_test[name]))
        CIR_test_comp[name].append(IR_imputer.transform(CIR_test[name]))
    MR_X_train_imp[name] = pd.DataFrame(np.mean(MR_X_train_comp[name], axis=0), columns=MR_col_names)
    MR_X_test_imp[name] = pd.DataFrame(np.mean(MR_X_test_comp[name], axis=0), columns=MR_col_names)
    CMR_test_imp[name] = pd.DataFrame(np.mean(CMR_test_comp[name], axis=0), columns=MR_col_names)
    IR_X_train_imp[name] = pd.DataFrame(np.mean(IR_X_train_comp[name], axis=0), columns=IR_col_names)
    IR_X_test_imp[name] = pd.DataFrame(np.mean(IR_X_test_comp[name], axis=0), columns=IR_col_names)
    CIR_test_imp[name] = pd.DataFrame(np.mean(CIR_test_comp[name], axis=0), columns=IR_col_names)
    
# imputation processing
def min_max_int(df, column, nb_cat=float('inf')):
    df[column] = round(df[column], 0)
    df[column][df[column] < 0] = 0
    df[column][df[column] > nb_cat - 1] = nb_cat - 1
    return df

def onehot(df, name):
    onehot = pd.DataFrame()
    for colonne in list(df.columns):
        if re.match(name, colonne):
            for indice in df[colonne][~df[colonne].isin([0, 1])].index:
                onehot.loc[indice, colonne] = df.loc[indice, colonne]
    i = 0
    for indice in onehot.index:
        onehot.loc[indice, list(onehot.idxmax(axis=1))[i]] = 1
        i += 1
    onehot[onehot!=1] = 0
    df.update(onehot)
    return df

MR_X_train_proc =  {}
MR_X_test_proc =  {}
CMR_test_proc =  {}
IR_X_train_proc =  {}
IR_X_test_proc =  {}
CIR_test_proc =  {}

def MR_imp_process(df):
    min_max_int(df, 'Years lived in place of residence', df['Current age'] + 1)
    min_max_int(df, 'Highest educational level', 4)
    onehot(df, 'Religion')
    min_max_int(df, 'Total number of years of education')
    min_max_int(df, 'Usual resident or visitor', 2)
    onehot(df, 'Relationship to household head')
    min_max_int(df, 'Age of household head', 100 + 1)
    min_max_int(df, 'Literacy', 3)
    min_max_int(df, 'Frequency of reading newspaper or magazine', 4)
    min_max_int(df, 'Frequency of listening to radio', 4)
    min_max_int(df, 'Frequency of watching television', 4)
    min_max_int(df, 'Times away from home in last 12 months', 146 + 1)
    min_max_int(df, 'Sons at home')
    min_max_int(df, 'Daughters at home')
    min_max_int(df, 'Sons elsewhere')
    min_max_int(df, 'Daughters elsewhere')
    min_max_int(df, 'Sons who have died')
    min_max_int(df, 'Daughters who have died')
    onehot(df, 'Knowledge of ovulatory cycle')
    min_max_int(df, 'Number of women fathered children with')
    min_max_int(df, 'Knowledge of any contraceptive method', 4)
    min_max_int(df, 'Current contraceptive method', 2)
    min_max_int(df, 'Current contraceptive by method type', 4)
    min_max_int(df, 'Heard family planning on radio last few months', 2)
    min_max_int(df, 'Heard family planning on TV last few months', 2)
    min_max_int(df, 'Heard family planning in newspaper/magazine last few months', 2)
    min_max_int(df, 'Discussed Family Planning with health worker in last few months', 2)
    min_max_int(df, 'Contraception is woman\'s business, man should not worry', 2)
    min_max_int(df, 'Women who use contraception become promiscuous', 2)
    min_max_int(df, 'Number of injections in last 12 months', 146 + 1)
    min_max_int(df, 'Covered by health insurance', 2)
    min_max_int(df, 'Respondent circumcised', 2)
    min_max_int(df, 'Time since last sex (in days)')
    min_max_int(df, 'Age at first sex (imputed)', df['Current age'] + 1)
    min_max_int(df, 'Recent sexual activity', 2)
    min_max_int(df, 'Fertility preference', 4)
    min_max_int(df, 'Ideal number of children')
    min_max_int(df, 'Ideal number of boys')
    min_max_int(df, 'Ideal number of girls')
    min_max_int(df, 'Ideal number of either sex')
    min_max_int(df, 'Wife justified refusing sex: husband has other women', 2)
    min_max_int(df, 'Currently working', 2)
    onehot(df, 'Occupation')
    min_max_int(df, 'Respondent worked in last 7 days', 3)
    min_max_int(df, 'Employment all year/seasonal', 4)
    min_max_int(df, 'Type of earnings from respondent\'s work', 4)
    min_max_int(df, 'Owns a house alone or jointly', 4)
    min_max_int(df, 'Owns land alone or jointly', 4)
    min_max_int(df, 'Condom used during last sex with most recent partner', 2)
    min_max_int(df, 'Had any STI in last 12 months', 2)
    min_max_int(df, 'Had genital sore/ulcer in last 12 months', 2)
    min_max_int(df, 'Had genital discharge in last 12 months', 2)
    min_max_int(df, 'Number of sex partners, including spouse, in last 12 months')
    min_max_int(df, 'Relationship with most recent sex partner', 6)
    min_max_int(df, 'Know a place to get HIV test', 2)
    min_max_int(df, 'Heard about other STIs', 2)
    min_max_int(df, 'Have ever paid anyone in exchange for sex', 2)
    min_max_int(df, 'Paid for sex in last 12 months', 2)
    min_max_int(df, 'Wife justified asking husband to use condom if he has STI', 2)
    min_max_int(df, 'Drugs to avoid HIV transmission to baby during pregnancy', 2)    
    min_max_int(df, 'Would buy vegetables from vendor with HIV', 2)
    min_max_int(df, 'Age of most recent partner', 100 + 1)
    df['Age of most recent partner'][df['Age of most recent partner'] < 10] = 10
    min_max_int(df, 'Total lifetime number of sex partners', 146 + 1)
    df['Total lifetime number of sex partners'][df['Total lifetime number of sex partners'] == 0] = 1
    min_max_int(df, 'Times in last 12 months had sex with most recent partner', 146 + 1)
    df['Times in last 12 months had sex with most recent partner'][df['Times in last 12 months had sex with most recent partner'] == 0] = 1
    min_max_int(df, 'Cluster altitude in meters')
    min_max_int(df, 'Reduce risk of getting HIV', 2)
    min_max_int(df, 'Ways of transmission from mother to child', 2)
    return df

for name in list(country_list):
    MR_X_train_proc[name] = MR_imp_process(MR_X_train_imp[name])
    MR_X_test_proc[name] = MR_imp_process(MR_X_test_imp[name])
    CMR_test_proc[name] = MR_imp_process(CMR_test_imp[name])

def IR_imp_process(df):
    min_max_int(df, 'Cluster altitude in meters')
    min_max_int(df, 'Years lived in place of residence', df['Current age'] + 1)
    min_max_int(df, 'Highest educational level', 4)
    min_max_int(df, 'Time to get to water source')
    min_max_int(df, 'Household has: electricity', 2)
    min_max_int(df, 'Household has: radio', 2)
    min_max_int(df, 'Household has: television', 2)
    min_max_int(df, 'Household has: refrigerator', 2)
    min_max_int(df, 'Household has: bicycle', 2)
    min_max_int(df, 'Household has: motorcycle/scooter', 2)
    min_max_int(df, 'Household has: car/truck', 2)
    onehot(df, 'Religion')
    min_max_int(df, 'Total number of years of education')
    min_max_int(df, 'Usual resident or visitor', 2)
    onehot(df, 'Relationship to household head')
    min_max_int(df, 'Age of household head', 100 + 1)
    min_max_int(df, 'Household has: telephone (land-line)', 2)
    min_max_int(df, 'Literacy', 3)
    min_max_int(df, 'Frequency of reading newspaper or magazine', 4)
    min_max_int(df, 'Frequency of listening to radio', 4)
    min_max_int(df, 'Frequency of watching television', 4)
    min_max_int(df, 'Toilet facilities shared with other households', 2)
    min_max_int(df, 'Times away from home in last 12 months', 146 + 1)
    min_max_int(df, 'Type of mosquito bed net(s) slept under last night', 4)
    min_max_int(df, 'Sons at home')
    min_max_int(df, 'Daughters at home')
    min_max_int(df, 'Sons elsewhere')
    min_max_int(df, 'Daughters elsewhere')
    min_max_int(df, 'Sons who have died')
    min_max_int(df, 'Daughters who have died')
    min_max_int(df, 'Births in last five years')
    min_max_int(df, 'Births in past year')
    min_max_int(df, 'Births in month of interview')
    min_max_int(df, 'Currently pregnant', 2)
    min_max_int(df, 'Menstruated in last six weeks', 2)
    onehot(df, 'Knowledge of ovulatory cycle')
    min_max_int(df, 'Entries in birth history')
    min_max_int(df, 'Ever had a terminated pregnancy', 2)
    min_max_int(df, 'Index last child prior to maternity-health (calendar)')    
    min_max_int(df, 'Births in last three years')
    min_max_int(df, 'Knowledge of any contraceptive method', 4)
    min_max_int(df, 'Ever used anything or tried to delay or avoid getting pregnant', 3)
    min_max_int(df, 'Current contraceptive method', 2)
    min_max_int(df, 'Current contraceptive by method type', 4)
    min_max_int(df, 'Pattern of contraceptive use', 4)
    min_max_int(df, 'Contraceptive use and intention', 4)
    min_max_int(df, 'Heard family planning on radio last few months', 2)
    min_max_int(df, 'Heard family planning on TV last few months', 2)
    min_max_int(df, 'Heard family planning in newspaper/magazine last few months', 2)
    min_max_int(df, 'Visited by fieldworker in last 12 months', 2)
    min_max_int(df, 'Visited health facility last 12 months', 2)
    min_max_int(df, 'Currently breastfeeding', 2)
    min_max_int(df, 'Currently amenorrheic', 2)
    min_max_int(df, 'Currently abstaining', 2)
    min_max_int(df, 'Heard of oral rehydration', 3)
    min_max_int(df, 'Rohrer\'s index')
    df['Rohrer\'s index'][df['Rohrer\'s index'] < 726] = 726
    min_max_int(df, 'Have mosquito bed net for sleeping', 2)
    min_max_int(df, 'Respondent slept under mosquito bed net', 2)
    min_max_int(df, 'Does not use cigarettes and tobacco', 2)
    min_max_int(df, 'Getting medical help for self: getting permission to go', 2)
    min_max_int(df, 'Getting medical help for self: getting money needed for treatment', 2)
    min_max_int(df, 'Getting medical help for self: distance to health facility', 2)
    min_max_int(df, 'Getting medical help for self: not wanting to go alone', 2)
    min_max_int(df, 'Number of injections in last 12 months', 146 + 1)
    min_max_int(df, 'Covered by health insurance', 2)
    min_max_int(df, 'Number of unions', 2)
    min_max_int(df, 'Age at first cohabitation', df['Current age'] + 1)
    min_max_int(df, 'Years since first cohabitation', df['Current age'] + 1)
    min_max_int(df, 'Time since last sex (in days)')
    min_max_int(df, 'Age at first sex (imputed)', df['Current age'] + 1)
    min_max_int(df, 'Recent sexual activity', 2)
    min_max_int(df, 'Fertility preference', 4)
    min_max_int(df, 'Ideal number of children')
    min_max_int(df, 'Fecund (definition 3)', 2)
    onehot(df, 'Unmet need for contraception')
    min_max_int(df, 'Ideal number of boys')
    min_max_int(df, 'Ideal number of girls')
    min_max_int(df, 'Ideal number of either sex')
    min_max_int(df, 'Wife justified refusing sex: husband has other women', 2)
    min_max_int(df, 'Currently working', 2)
    onehot(df, 'Occupation')
    min_max_int(df, 'Respondent worked in last 7 days', 4)
    min_max_int(df, 'Owns a house alone or jointly', 4)
    min_max_int(df, 'Owns land alone or jointly', 4)
    min_max_int(df, 'Ever heard of a Sexually Transmitted Infection (STI)', 2)
    min_max_int(df, 'Condom used during last sex with most recent partner', 2)
    min_max_int(df, 'Had any STI in last 12 months', 2)
    min_max_int(df, 'Had genital sore/ulcer in last 12 months', 2)
    min_max_int(df, 'Had genital discharge in last 12 months', 2)
    min_max_int(df, 'Number of sex partners, including spouse, in last 12 months')
    min_max_int(df, 'Relationship with most recent sex partner', 6)
    min_max_int(df, 'Ever been tested for HIV', 2)
    min_max_int(df, 'Know a place to get HIV test', 2)
    min_max_int(df, 'Heard about other STIs', 2)
    min_max_int(df, 'Wife justified asking husband to use condom if he has STI', 2)
    min_max_int(df, 'Drugs to avoid HIV transmission to baby during pregnancy', 2)   
    min_max_int(df, 'Would buy vegetables from vendor with HIV', 2)
    min_max_int(df, 'Age of most recent partner', 100 + 1)
    df['Age of most recent partner'][df['Age of most recent partner'] < 10] = 10
    min_max_int(df, 'Total lifetime number of sex partners', 146 + 1)
    df['Total lifetime number of sex partners'][df['Total lifetime number of sex partners'] == 0] = 1
    min_max_int(df, 'Times in last 12 months had sex with most recent partner', 146 + 1)
    df['Times in last 12 months had sex with most recent partner'][df['Times in last 12 months had sex with most recent partner'] == 0] = 1
    min_max_int(df, 'Beating justified', 2)
    min_max_int(df, 'Reduce risk of getting HIV', 2)
    min_max_int(df, 'Ways of transmission from mother to child', 2)
    min_max_int(df, 'Presence of other people for \'Wife beating justified\' questions', 2)
    min_max_int(df, 'Presence of other people during the sexual activity section of the interview', 2)
    return df

for name in list(country_list):
    IR_X_train_proc[name] = IR_imp_process(IR_X_train_imp[name])
    IR_X_test_proc[name] = IR_imp_process(IR_X_test_imp[name])
    CIR_test_proc[name] = IR_imp_process(CIR_test_imp[name])
    
MR_X_train_ready = {}
MR_X_test_ready = {}
CMR_test_ready = {}
IR_X_train_ready = {}
IR_X_test_ready = {}
CIR_test_ready = {}

# standardization of the data
for name in list(country_list):
    MR_scaler = StandardScaler(with_mean=False)
    MR_X_train_ready[name] = round(pd.DataFrame(MR_scaler.fit_transform(MR_X_train_proc[name]), columns=MR_col_names), 2)
    MR_X_test_ready[name] = round(pd.DataFrame(MR_scaler.transform(MR_X_test_proc[name]), columns=MR_col_names), 2)
    CMR_test_ready[name] = round(pd.DataFrame(MR_scaler.transform(CMR_test_proc[name]), columns=MR_col_names), 2)
    MR_var = MR_scaler.var_
    IR_scaler = StandardScaler(with_mean=False)
    IR_X_train_ready[name] = round(pd.DataFrame(IR_scaler.fit_transform(IR_X_train_proc[name]), columns=IR_col_names), 2)
    IR_X_test_ready[name] = round(pd.DataFrame(IR_scaler.transform(IR_X_test_proc[name]), columns=IR_col_names), 2)
    CIR_test_ready[name] = round(pd.DataFrame(IR_scaler.transform(CIR_test_proc[name]), columns=IR_col_names), 2)
    IR_var = IR_scaler.var_
    
# pickling data
f = open("data/processed/MR_X_train_imp.pkl", 'wb')
pickle.dump(MR_X_train_imp, f)
f = open("data/processed/MR_X_test_imp.pkl", 'wb')
pickle.dump(MR_X_test_imp, f)
f = open("data/processed/CMR_test_imp.pkl", 'wb')
pickle.dump(CMR_test_imp, f)
f = open("data/processed/IR_X_train_imp.pkl", 'wb')
pickle.dump(IR_X_train_imp, f)
f = open("data/processed/IR_X_test_imp.pkl", 'wb')
pickle.dump(IR_X_test_imp, f)
f = open("data/processed/CIR_test_imp.pkl", 'wb')
pickle.dump(CIR_test_imp, f)
f = open("data/processed/MR_X_train_comp.pkl", 'wb')
pickle.dump(MR_X_train_comp, f)
f = open("data/processed/MR_X_test_comp.pkl", 'wb')
pickle.dump(MR_X_test_comp, f)
f = open("data/processed/CMR_test_comp.pkl", 'wb')
pickle.dump(CMR_test_comp, f)
f = open("data/processed/IR_X_train_comp.pkl", 'wb')
pickle.dump(IR_X_train_comp, f)
f = open("data/processed/IR_X_test_comp.pkl", 'wb')
pickle.dump(IR_X_test_comp, f)
f = open("data/processed/CIR_test_comp.pkl", 'wb')
pickle.dump(CIR_test_comp, f)
f = open("data/processed/MR_X_train_proc.pkl", 'wb')
pickle.dump(MR_X_train_proc, f)
f = open("data/processed/MR_X_test_proc.pkl", 'wb')
pickle.dump(MR_X_test_proc, f)
f = open("data/processed/CMR_test_proc.pkl", 'wb')
pickle.dump(CMR_test_proc, f)
f = open("data/processed/IR_X_train_proc.pkl", 'wb')
pickle.dump(IR_X_train_proc, f)
f = open("data/processed/IR_X_test_proc.pkl", 'wb')
pickle.dump(IR_X_test_proc, f)
f = open("data/processed/CIR_test_proc.pkl", 'wb')
pickle.dump(CIR_test_proc, f)
f = open("data/processed/MR_X_train_ready.pkl", 'wb')
pickle.dump(MR_X_train_ready, f)
f = open("data/processed/MR_X_test_ready.pkl", 'wb')
pickle.dump(MR_X_test_ready, f)
f = open("data/processed/CMR_test_ready.pkl", 'wb')
pickle.dump(CMR_test_ready, f)
f = open("data/processed/IR_X_train_ready.pkl", 'wb')
pickle.dump(IR_X_train_ready, f)
f = open("data/processed/IR_X_test_ready.pkl", 'wb')
pickle.dump(IR_X_test_ready, f)
f = open("data/processed/CIR_test_ready.pkl", 'wb')
pickle.dump(CIR_test_ready, f)
f = open("data/processed/MR_Y_train.pkl", 'wb')
pickle.dump(MR_Y_train, f)
f = open("data/processed/MR_Y_test.pkl", 'wb')
pickle.dump(MR_Y_test, f)
f = open("data/processed/CMR_Y_test.pkl", 'wb')
pickle.dump(CMR_Y_test, f)
f = open("data/processed/IR_Y_train.pkl", 'wb')
pickle.dump(IR_Y_train, f)
f = open("data/processed/IR_Y_test.pkl", 'wb')
pickle.dump(IR_Y_test, f)
f = open("data/processed/CIR_Y_test.pkl", 'wb')
pickle.dump(CIR_Y_test, f)
f = open("data/processed/MR_var.pkl", 'wb')
pickle.dump(MR_var, f)
f = open("data/processed/IR_var.pkl", 'wb')
pickle.dump(IR_var, f)
f.close()