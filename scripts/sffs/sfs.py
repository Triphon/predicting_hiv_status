# import libraries
import os
import pandas as pd
import numpy as np
import pickle
from joblib import dump, load
#from sklearn.model_selection import StratifiedKFold
from xgboost import XGBClassifier
from mlxtend.feature_selection import SequentialFeatureSelector as SFS

# specifying constant parameters
random_seed = 5

# set working directory
working_directory = "/home/oreler/"
os.chdir(working_directory)

# code for uploading pickled data
#MR_X_train_entire_ready = pickle.load(open("Train_samples/MR_X_train_entire_ready.pkl", 'rb'))
#MR_Y_train_entire_ready = pickle.load(open("Train_samples/MR_Y_train_entire_ready.pkl", 'rb'))
IR_X_train_entire = pickle.load(open("Pure_data/MR_X_train_entire.pkl", 'rb'))
IR_Y_train_entire = pickle.load(open("Pure_data/MR_Y_train_entire.pkl", 'rb'))

# stratified k-fold and model initialization
#kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=random_seed)

#MR_model = load("xgb_entire_mice/MR_xgb_entire_mice.joblib")
IR_model = load("xgb_entire_pure_data/MR_xgb_entire_pure_data.joblib")

#MR_sfs = SFS(MR_model.best_estimator_, k_features=20, forward=True, floating=False, scoring='f1', verbose=1, cv=kfold, n_jobs=-1)
IR_sfs = SFS(IR_model.best_estimator_, k_features=30, forward=True, floating=True, scoring='f1', verbose=1, cv=0, n_jobs=-1)

#MR_sfs.fit(MR_X_train_entire_ready, MR_Y_train_entire_ready)
IR_sfs.fit(IR_X_train_entire, IR_Y_train_entire)

#dump(MR_sfs, 'MR_sfs.joblib')
dump(IR_sfs, 'MR_sfs_pure.joblib')
