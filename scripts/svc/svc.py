# import libraries
import os
import pandas as pd
import numpy as np
import pickle
from joblib import dump, load
from sklearn.model_selection import StratifiedKFold, RandomizedSearchCV
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score, brier_score_loss
from sklearn.svm import SVC

# specifying constant parameters
random_seed = 5

# set working directory
working_directory = '/home/oreler/predicting_hiv_status'
os.chdir(working_directory)

# code for uploading pickled data
MR_X_train_ready = pickle.load(open('data/Train_samples/MR_X_train_ready.pkl', 'rb'))
IR_X_train_ready = pickle.load(open('data/Train_samples/IR_X_train_ready.pkl', 'rb'))
MR_Y_train = pickle.load(open('data/Train_samples/MR_Y_train.pkl', 'rb'))
IR_Y_train = pickle.load(open('data/Train_samples/IR_Y_train.pkl', 'rb'))

country_list = ['Angola', 'Burundi', 'Ethiopia', 'Lesotho', 'Malawi', 'Mozambique', 'Namibia', 'Rwanda', 'Zambia', 'Zimbabwe']

#================================================================================================================================
# parameters space, models definition and random grid search
#================================================================================================================================

# stratified k-fold and model initialization
kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=random_seed)

scoring = ['neg_brier_score', 'f1', 'recall', 'precision']

# support vector machine space
params_svc = {'C': np.logspace(-9, 9)}

svc_model = RandomizedSearchCV(estimator=SVC(gamma='scale',
                                             random_state=random_seed,
                                             class_weight='balanced',
                                             kernel='rbf',
                                             probability=True
                                            ),
                               param_distributions=params_svc,
                               n_iter=50,
                               scoring=scoring,
                               cv=kfold,
                               n_jobs=-1,
                               random_state=random_seed,
                               verbose=2,
                               refit='neg_brier_score'
                              )

for name in list(country_list):
    
    svc_model.fit(MR_X_train_ready[name], MR_Y_train[name])
    dump(svc_model, "scripts/svc/new/MR_svc_" + name + '.joblib')
    
    svc_model.fit(IR_X_train_ready[name], IR_Y_train[name])
    dump(svc_model, "scripts/svc/new/IR_svc_" + name + '.joblib')