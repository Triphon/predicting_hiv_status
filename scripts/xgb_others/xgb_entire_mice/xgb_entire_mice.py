# import libraries
import os
import pandas as pd
import numpy as np
import pickle
from joblib import dump, load
from matplotlib import pyplot
from sklearn.model_selection import StratifiedKFold, RandomizedSearchCV
from sklearn.metrics import confusion_matrix, f1_score, recall_score, precision_score
from xgboost import XGBClassifier

# specifying constant parameters
random_seed = 5

# set working directory
working_directory = "/home/oreler/"
os.chdir(working_directory)

# code for uploading pickled data
MR_X_train_entire_ready = pickle.load(open("Train_samples/MR_X_train_entire_ready.pkl", 'rb'))
MR_Y_train_entire_ready = pickle.load(open("Train_samples/MR_Y_train_entire_ready.pkl", 'rb'))
IR_X_train_entire_ready = pickle.load(open("Train_samples/IR_X_train_entire_ready.pkl", 'rb'))
IR_Y_train_entire_ready = pickle.load(open("Train_samples/IR_Y_train_entire_ready.pkl", 'rb'))

#================================================================================================================================
# parameters space, models definition and random grid search
#================================================================================================================================

# stratified k-fold and model initialization
kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=random_seed)
scoring = ['f1', 'recall', 'precision']

# XGBoost space
params_xgb = {'learning_rate': np.linspace(0, 1, 11),
              'min_split_loss': np.linspace(0, 1, 6),
              'max_depth': np.linspace(2, 10, 5, dtype=int),
              'min_child_weight': np.linspace(1, 20, 20, dtype=int),
              'colsample_bytree': np.linspace(0.5, 1, 6),
              'reg_alpha': np.linspace(0, 1, 11),
              'scale_pos_weight': np.linspace(4, 50, 24, dtype=int),
              'n_estimators': np.linspace(10, 450, 12, dtype=int),
              'reg_lambda': np.linspace(0, 10, 11),
             }

MR_xgb_entire_mice = RandomizedSearchCV(estimator=XGBClassifier(booster='gbtree',
                                                                     objective='binary:logistic',
                                                                     random_state=random_seed,
                                                                     nthread=1),
                                             param_distributions=params_xgb,
                                             n_iter=250,
                                             scoring=scoring,
                                             cv=kfold,
                                             n_jobs=-1,
                                             random_state=random_seed,
                                             verbose=1,
                                             refit='f1')

MR_xgb_entire_mice.fit(MR_X_train_entire_ready, MR_Y_train_entire_ready)
dump(MR_xgb_entire_mice, "xgb_entire_mice/MR_xgb_entire_mice.joblib")

IR_xgb_entire_mice = RandomizedSearchCV(estimator=XGBClassifier(booster='gbtree',
                                                                     objective='binary:logistic',
                                                                     random_state=random_seed,
                                                                     nthread=1),
                                             param_distributions=params_xgb,
                                             n_iter=250,
                                             scoring=scoring,
                                             cv=kfold,
                                             n_jobs=-1,
                                             random_state=random_seed,
                                             verbose=1,
                                             refit='f1')
    
IR_xgb_entire_mice.fit(IR_X_train_entire_ready, IR_Y_train_entire_ready)
dump(IR_xgb_entire_mice, "xgb_entire_mice/IR_xgb_entire_mice.joblib")