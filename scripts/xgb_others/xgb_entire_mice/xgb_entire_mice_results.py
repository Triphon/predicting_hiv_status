# import libraries
import os
import pandas as pd
import numpy as np
import pickle
from joblib import dump, load
from matplotlib import pyplot
import shap
from sklearn.model_selection import StratifiedKFold, RandomizedSearchCV
from sklearn.metrics import confusion_matrix, f1_score, recall_score, precision_score, precision_recall_curve, auc
from xgboost import XGBClassifier

# specifying constant parameters
random_seed = 5

# set working directory
working_directory = r"C:\Users\oreler\Documents\Python_Local"
os.chdir(working_directory)

# code for uploading pickled data
MR_X_train_entire_ready = pickle.load(open("Train_samples\MR_X_train_entire_ready.pkl", 'rb'))
MR_Y_train_entire_ready = pickle.load(open("Train_samples\MR_Y_train_entire_ready.pkl", 'rb'))
IR_X_train_entire_ready = pickle.load(open("Train_samples\IR_X_train_entire_ready.pkl", 'rb'))
IR_Y_train_entire_ready = pickle.load(open("Train_samples\IR_Y_train_entire_ready.pkl", 'rb'))

MR_X_test_entire_ready = pickle.load(open("Test_samples\MR_X_test_entire_ready.pkl", 'rb'))
MR_Y_test_entire_ready = pickle.load(open("Test_samples\MR_Y_test_entire_ready.pkl", 'rb'))
IR_X_test_entire_ready = pickle.load(open("Test_samples\IR_X_test_entire_ready.pkl", 'rb'))
IR_Y_test_entire_ready = pickle.load(open("Test_samples\IR_Y_test_entire_ready.pkl", 'rb'))

MR_xgb_entire_mice = load("xgb_entire_mice\MR_xgb_entire_mice.joblib")
IR_xgb_entire_mice = load("xgb_entire_mice\IR_xgb_entire_mice.joblib")

MR_best_index = MR_xgb_entire_mice.best_index_
IR_best_index = IR_xgb_entire_mice.best_index_

MR_Y_train_entire_ready_pred = MR_xgb_entire_mice.predict(MR_X_train_entire_ready)
# MR_Y_train_entire_ready_pred_thresh = (MR_xgb_entire_mice.predict_proba(MR_X_train_entire_ready)[:,1] >= 0.3).astype(bool) # set threshold as 0.3
IR_Y_train_entire_ready_pred = IR_xgb_entire_mice.predict(IR_X_train_entire_ready)

print('Confusion matrix on the train dataset:', confusion_matrix(MR_Y_train_entire_ready, MR_Y_train_entire_ready_pred))
print('Average F1 score on the train sample', "{:.1%}".format(MR_xgb_entire_mice.cv_results_['mean_test_f1'][MR_best_index]))
print('CI F1 score on the train sample', "{:.1%}".format(2*MR_xgb_entire_mice.cv_results_['std_test_f1'][MR_best_index]))
print('Average Sensitivity on the train sample', "{:.1%}".format(MR_xgb_entire_mice.cv_results_['mean_test_recall'][MR_best_index]))
print('CI Sensitivity on the train sample', "{:.1%}".format(2*MR_xgb_entire_mice.cv_results_['std_test_recall'][MR_best_index]))
print('Average Positive Predicted Value on the train sample', "{:.1%}".format(MR_xgb_entire_mice.cv_results_['mean_test_precision'][MR_best_index]))
print('CI Positive Predicted Value on the train sample', "{:.1%}".format(2*MR_xgb_entire_mice.cv_results_['std_test_precision'][MR_best_index]))
print('Parameters', MR_xgb_entire_mice.best_estimator_)

print('Confusion matrix on the train dataset:', confusion_matrix(IR_Y_train_entire_ready, IR_Y_train_entire_ready_pred))
print('Average F1 score on the train sample', "{:.1%}".format(IR_xgb_entire_mice.cv_results_['mean_test_f1'][IR_best_index]))
print('CI F1 score on the train sample', "{:.1%}".format(2*IR_xgb_entire_mice.cv_results_['std_test_f1'][IR_best_index]))
print('Average Sensitivity on the train sample', "{:.1%}".format(IR_xgb_entire_mice.cv_results_['mean_test_recall'][IR_best_index]))
print('CI Sensitivity on the train sample', "{:.1%}".format(2*IR_xgb_entire_mice.cv_results_['std_test_recall'][IR_best_index]))
print('Average Positive Predicted Value on the train sample', "{:.1%}".format(IR_xgb_entire_mice.cv_results_['mean_test_precision'][IR_best_index]))
print('CI Positive Predicted Value on the train sample', "{:.1%}".format(2*IR_xgb_entire_mice.cv_results_['std_test_precision'][IR_best_index]))
print('Parameters', IR_xgb_entire_mice.best_estimator_)
     
MR_Y_test_entire_ready_pred = MR_xgb_entire_mice.predict(MR_X_test_entire_ready)
IR_Y_test_entire_ready_pred = IR_xgb_entire_mice.predict(IR_X_test_entire_ready)

print('Confusion matrix on the test dataset:', confusion_matrix(MR_Y_test_entire_ready, MR_Y_test_entire_ready_pred))
print('F1 score on the test dataset:', "{:.1%}".format(f1_score(MR_Y_test_entire_ready, MR_Y_test_entire_ready_pred)))
print('Sensitivity on the test dataset:', "{:.1%}".format(recall_score(MR_Y_test_entire_ready, MR_Y_test_entire_ready_pred)))
print('Positive Predicted Value on the test dataset:', "{:.1%}".format(precision_score(MR_Y_test_entire_ready, MR_Y_test_entire_ready_pred)))

print('Confusion matrix on the test dataset:', confusion_matrix(IR_Y_test_entire_ready, IR_Y_test_entire_ready_pred))
print('F1 score on the test dataset:', "{:.1%}".format(f1_score(IR_Y_test_entire_ready, IR_Y_test_entire_ready_pred)))
print('Sensitivity on the test dataset:', "{:.1%}".format(recall_score(IR_Y_test_entire_ready, IR_Y_test_entire_ready_pred)))
print('Positive Predicted Value on the test dataset:', "{:.1%}".format(precision_score(IR_Y_test_entire_ready, IR_Y_test_entire_ready_pred)))

def pr_f1_curve(model, dx_test, dy_test):
     dy_test_predict_proba = model.predict_proba(dx_test)[:,1]
     dy_test_predict = model.predict(dx_test)
     ppv_curve, sensitivity_curve, _ = precision_recall_curve(dy_test, dy_test_predict_proba)
     F1_score, AUC_score = f1_score(dy_test, dy_test_predict), auc(sensitivity_curve, ppv_curve)
     print(': F1 score = %.2f - Area under Curve = %.2f' % (F1_score, AUC_score))
     prevalence = np.sum(dy_test)/len(dy_test)
     pyplot.plot([0, 1], [prevalence, prevalence], linestyle='--', label='Random Testing')
     pyplot.plot(sensitivity_curve, ppv_curve, marker='.', label='Algorithmic Testing')
     pyplot.plot(sensitivity_curve, 2*(sensitivity_curve*ppv_curve)/(sensitivity_curve+ppv_curve), marker='', label='F1')
     pyplot.xlabel('Sensitivity')
     pyplot.ylabel('Positive Predicted Value')
     pyplot.legend()
     pyplot.show()

pr_f1_curve(MR_xgb_entire_mice, MR_X_test_entire_ready, MR_Y_test_entire_ready)
pr_f1_curve(IR_xgb_entire_mice, IR_X_test_entire_ready, IR_Y_test_entire_ready)

# Explain model predictions using shapley value
MR_explainer = shap.TreeExplainer(MR_xgb_entire_mice.best_estimator_)
MR_shap_values = MR_explainer.shap_values(MR_X_train_entire_ready)
IR_explainer = shap.TreeExplainer(IR_xgb_entire_mice.best_estimator_)
IR_shap_values = IR_explainer.shap_values(IR_X_train_entire_ready)

# Plot summary_plot
shap.summary_plot(MR_shap_values, MR_X_train_entire_ready, max_display=30)
shap.summary_plot(IR_shap_values, IR_X_train_entire_ready, max_display=30)