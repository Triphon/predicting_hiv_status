# import libraries
import os
import pandas as pd
import numpy as np
import pickle
from joblib import dump, load
from matplotlib import pyplot
from sklearn.model_selection import StratifiedKFold, RandomizedSearchCV
from sklearn.metrics import f1_score, recall_score, precision_score
from xgboost import XGBClassifier

# specifying constant parameters
random_seed = 5

# set working directory
working_directory = working_directory = "/home/oreler/"
os.chdir(working_directory)

# code for uploading pickled data
MR_X_train_entire = pickle.load(open("Pure_data/MR_X_train_entire.pkl", 'rb'))
IR_X_train_entire = pickle.load(open("Pure_data/IR_X_train_entire.pkl", 'rb'))

#================================================================================================================================
# parameters space, models definition and random grid search
#===============================================================================================================================
# split between train (80%) and test (20%) with stratification

for name in list(country_list):  
    MR_X_train[name], MR_X_test[name], MR_Y_train[name], MR_Y_test[name] = train_test_split(CMR_train[name], CMR_Y_train[name], test_size=0.2, stratify=CMR_Y_train[name], random_state=random_seed)
    IR_X_train[name], IR_X_test[name], IR_Y_train[name], IR_Y_test[name] = train_test_split(CIR_train[name], CIR_Y_train[name], test_size=0.2, stratify=CIR_Y_train[name], random_state=random_seed)


# stratified k-fold and model initialization
kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=random_seed)
scoring = ['f1', 'recall', 'precision']

# XGBoost space
params_xgb = {'learning_rate': np.linspace(0, 1, 11),
              'min_split_loss': np.linspace(0, 1, 6),
              'max_depth': np.linspace(2, 10, 5, dtype=int),
              'min_child_weight': np.linspace(1, 20, 20, dtype=int),
              'colsample_bytree': np.linspace(0.5, 1, 6),
              'reg_alpha': np.linspace(0, 1, 11),
              'scale_pos_weight': np.linspace(4, 50, 24, dtype=int),
              'n_estimators': np.linspace(10, 450, 12, dtype=int),
              'reg_lambda': np.linspace(0, 10, 11),
             }

MR_xgb_entire_pure_data = RandomizedSearchCV(estimator=XGBClassifier(booster='gbtree',
                                                                     objective='binary:logistic',
                                                                     random_state=random_seed,
                                                                     nthread=1),
                                             param_distributions=params_xgb,
                                             n_iter=250,
                                             scoring=scoring,
                                             cv=kfold,
                                             n_jobs=-1,
                                             random_state=random_seed,
                                             verbose=1,
                                             refit='f1')

MR_xgb_entire_pure_data.fit(MR_X_train_entire, MR_Y_train_entire)
dump(MR_xgb_entire_pure_data, "xgb_entire_pure_data/MR_xgb_entire_pure_data.joblib")

IR_xgb_entire_pure_data = RandomizedSearchCV(estimator=XGBClassifier(booster='gbtree',
                                                                     objective='binary:logistic',
                                                                     random_state=random_seed,
                                                                     nthread=1),
                                             param_distributions=params_xgb,
                                             n_iter=250,
                                             scoring=scoring,
                                             cv=kfold,
                                             n_jobs=-1,
                                             random_state=random_seed,
                                             verbose=1,
                                             refit='f1')
    
IR_xgb_entire_pure_data.fit(IR_X_train_entire, IR_Y_train_entire)
dump(IR_xgb_entire_pure_data, "xgb_entire_pure_data/IR_xgb_entire_pure_data.joblib")