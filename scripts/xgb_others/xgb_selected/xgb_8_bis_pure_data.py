# import libraries
import os
import pandas as pd
import numpy as np
import pickle
from joblib import dump, load
from sklearn.model_selection import StratifiedKFold, RandomizedSearchCV
from sklearn.metrics import f1_score, recall_score, precision_score
from xgboost import XGBClassifier

# specifying constant parameters
random_seed = 5

# set working directory
working_directory = r"C:\Users\oreler\Documents\Python_Local"
os.chdir(working_directory)

# code for uploading pickled data
MR_X_train_entire = pickle.load(open("Pure_data\MR_X_train_entire.pkl", 'rb'))
MR_Y_train_entire = pickle.load(open("Pure_data\MR_Y_train_entire.pkl", 'rb'))

IR_X_train_entire = pickle.load(open("Pure_data\IR_X_train_entire.pkl", 'rb'))
IR_Y_train_entire = pickle.load(open("Pure_data\IR_Y_train_entire.pkl", 'rb'))

# select features
MR_selected_features = ['Cluster\'s latitude coordinate', 'Current age', 'Cluster\'s longitude coordinate', 'Total lifetime number of sex partners', 'Age of most recent partner', 'Wealth index factor score combined', 'Years lived in place of residence', 'Condom used during last sex with most recent partner']

IR_selected_features = ['Cluster\'s latitude coordinate', 'Current age', 'Cluster\'s longitude coordinate', 'Total lifetime number of sex partners', 'Age of most recent partner', 'Wealth index factor score combined', 'Years lived in place of residence', 'Condom used during last sex with most recent partner']

MR_X_train_selected = MR_X_train_entire[MR_selected_features]
IR_X_train_selected = IR_X_train_entire[IR_selected_features]

#================================================================================================================================
# parameters space, models definition and random grid search
#================================================================================================================================

# stratified k-fold and model initialization
kfold = StratifiedKFold(n_splits=5, shuffle=True, random_state=random_seed)
scoring = ['f1', 'recall', 'precision']

# XGBoost space
params_xgb = {'learning_rate': np.linspace(0, 1, 11),
              'min_split_loss': np.linspace(0, 1, 6),
              'max_depth': np.linspace(2, 10, 5, dtype=int),
              'min_child_weight': np.linspace(1, 20, 20, dtype=int),
              'colsample_bytree': np.linspace(0.5, 1, 6),
              'reg_alpha': np.linspace(0, 1, 11),
              'scale_pos_weight': np.linspace(4, 50, 24, dtype=int),
              'n_estimators': np.linspace(10, 450, 12, dtype=int),
              'reg_lambda': np.linspace(0, 10, 11),
             }

MR_xgb_selected = RandomizedSearchCV(estimator=XGBClassifier(booster='gbtree',
                                                                     objective='binary:logistic',
                                                                     random_state=random_seed,
                                                                     nthread=1,
                                                                     verbosity=1),
                                             param_distributions=params_xgb,
                                             n_iter=250,
                                             scoring=scoring,
                                             cv=kfold,
                                             n_jobs=-1,
                                             random_state=random_seed,
                                             verbose=2,
                                             refit='f1')

MR_xgb_selected.fit(MR_X_train_selected, MR_Y_train_entire)
dump(MR_xgb_selected, "xgb_selected\MR_xgb_selected_8_bis.joblib")

IR_xgb_selected = RandomizedSearchCV(estimator=XGBClassifier(booster='gbtree',
                                                                     objective='binary:logistic',
                                                                     random_state=random_seed,
                                                                     nthread=1,
                                                                     verbosity=1),
                                             param_distributions=params_xgb,
                                             n_iter=250,
                                             scoring=scoring,
                                             cv=kfold,
                                             n_jobs=-1,
                                             random_state=random_seed,
                                             verbose=2,
                                             refit='f1')
    
IR_xgb_selected.fit(IR_X_train_selected, IR_Y_train_entire)
dump(IR_xgb_selected, "xgb_selected\IR_xgb_selected_8_bis.joblib")